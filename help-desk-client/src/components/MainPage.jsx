import React from "react";
import TabPanel from "./TabPanel";
import TicketsTable from "./TicketsTable";
import {
    AppBar,
    Button,
    ButtonGroup,
    MenuItem,
    Select,
    Tab,
    Tabs,
    Typography
} from "@material-ui/core";
import TicketInfoWithRouter from "./TicketInfo";
import {Link, Switch, Route, withRouter} from "react-router-dom";

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        "aria-controls": `full-width-tabpanel-${index}`,
    };
}

class MainPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ticketsQuantity: 0,
            pageArray: [5, 10, 25, 100],
            prop: 42,
            tabValue: 0,
            tTickets: [],
            searchParam: {
                pageNumber: 1,
                pageSize: 5,
                sortField: "ID",
                sortDirection: "ASC",
                filterValue: "",
                myTicket: true,
            },

        };
    }

    objToQueryString(obj) {
        const keyValuePairs = [];
        for (const key in obj) {
            keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
        }
        return keyValuePairs.join('&');
    }

    componentDidMount() {

        const queryString = this.objToQueryString(
            this.state.searchParam
        );

        fetch(`http://localhost:8080/tickets/{pageNumber}/{pageSize}/{sortField}/{sortDirection}/{filterValue}/{myTicket}?${queryString}`, {
            method: 'GET',
            headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then((responseJson) => {
                this.setState({
                    tTickets: responseJson.tickets,
                    ticketsQuantity: responseJson.ticketsQuantity
                })
            })
    }

    handleLogout = () => {
        this.props.authCallback(false);
        localStorage.clear();
    };

    handleTabChange = (event, value) => {
        this.state.searchParam.myTicket = value === 0;
        this.state.searchParam.filterValue = "";
        this.state.searchParam.pageSize = 5;
        this.state.searchParam.pageNumber = 1;
        this.state.searchParam.sortField = "ID";
        this.state.searchParam.sortDirection = "ASC";

        this.setState({
            tabValue: value,
            filteredTickets: []
        });

        this.componentDidMount();

    };

    handlePageLeft = () => {

        if (this.state.searchParam.pageNumber > 1 ){
            this.state.searchParam.pageNumber = this.state.searchParam.pageNumber - 1;
        }
        this.componentDidMount();
    };

    handlePageRight = () => {

        if (this.state.searchParam.pageNumber < Math.ceil(this.state.ticketsQuantity / this.state.searchParam.pageSize)){
            this.state.searchParam.pageNumber = this.state.searchParam.pageNumber + 1;
        }
        this.componentDidMount();
    };

    handleSortField = (fieldName) => {

        if (fieldName === "ID") {
            this.state.searchParam.sortField = "ID"
        } else if (fieldName === "Name") {
            this.state.searchParam.sortField = "NAME"
        } else if (fieldName === "Desired Date") {
            this.state.searchParam.sortField = "DESIRED_RESOLUTION_DATE"
        } else if (fieldName === "Urgency") {
            this.state.searchParam.sortField = "URGENCY"
        }

        if (this.state.searchParam.sortDirection === "ASC") {
            this.state.searchParam.sortDirection = "DESC"
        } else {
            this.state.searchParam.sortDirection = "ASC"
        }
        this.componentDidMount();
    };

    handleFromTableTicket = (event) => {

        this.state.searchParam.pageSize = event.target.value;

        this.componentDidMount();
    };

    handleSearchTicket = (event) => {

        this.state.searchParam.filterValue = event.target.value;

        this.componentDidMount();
    };


    render() {
        const {tabValue, tTickets, pageArray, ticketsQuantity} = this.state;
        const {
            pageNumber,
            pageSize
        } = this.state.searchParam;
        const {path} = this.props.match;
        const {
            handleSearchTicket,
            handleFromTableTicket,
            handleSortField,
            handlePageRight,
            handlePageLeft
        } = this;

        return (
            <>
                <Switch>
                    <Route exact path={path}>
                        <div className="buttons-container">
                            <Button
                                component={Link}
                                to="/create-ticket"
                                onClick={this.handleCreate}
                                variant="contained"
                                color="primary"
                            >
                                Create Ticket
                            </Button>
                            <Button
                                component={Link}
                                to="/"
                                onClick={this.handleLogout}
                                variant="contained"
                                color="secondary"
                            >
                                Logout
                            </Button>
                        </div>
                        <div className="table-container">
                            <AppBar position="static">
                                <Tabs
                                    variant="fullWidth"
                                    onChange={this.handleTabChange}
                                    value={tabValue}
                                >
                                    <Tab label="My tickets" {...a11yProps(0)} />
                                    <Tab label="All tickets" {...a11yProps(1)} />
                                </Tabs>
                                <TabPanel value={tabValue} index={0}>
                                    <TicketsTable
                                        searchCallback={handleSearchTicket}
                                        tickets={tTickets}
                                        rowsPerPage={pageSize}
                                        ticketsQuantity={ticketsQuantity}
                                        sortField={handleSortField}
                                    />
                                </TabPanel>
                                <TabPanel value={tabValue} index={1}>
                                    <TicketsTable
                                        searchCallback={handleSearchTicket}
                                        tickets={tTickets}
                                        rowsPerPage={pageSize}
                                        ticketsQuantity={ticketsQuantity}
                                        sortField={handleSortField}
                                    />
                                </TabPanel>
                                <div className="ticket-data-container__pagination">
                                    <Typography align="center" variant="h7">
                                        Rows per page
                                    </Typography>
                                    <Select
                                        value={pageSize}
                                        onChange={handleFromTableTicket}
                                        className={"ticket-creation-input_width80"}
                                    >
                                        {pageArray.map((item, index) => {
                                            return (
                                                <MenuItem value={item} key={index}>
                                                    {item}
                                                </MenuItem>
                                            );
                                        })}
                                    </Select>
                                    <ButtonGroup variant="contained" color="primary">
                                        <Button
                                            onClick={handlePageLeft}
                                        >
                                            &lt;
                                        </Button>

                                        <Button
                                            onClick={handlePageRight}
                                        >
                                            &gt;
                                        </Button>
                                    </ButtonGroup>
                                    <Typography align="center" variant="h7">
                                        Page: {pageNumber}
                                    </Typography>
                                    <Typography align="left" variant="h7">
                                        Total pages: {Math.ceil(ticketsQuantity / pageSize)}
                                    </Typography>
                                </div>
                            </AppBar>
                        </div>
                    </Route>
                    <Route path={`${path}/:ticketId`}>
                        <TicketInfoWithRouter/>
                    </Route>
                </Switch>
            </>
        );
    }
}

const MainPageWithRouter = withRouter(MainPage);
export default MainPageWithRouter;