import React from "react";
import PropTypes from "prop-types";
import CommentsTable from "./CommentsTable";
import HistoryTable from "./HistoryTable";
import TabPanel from "./TabPanel";
import TicketCreationPageWithRouter from "./TicketCreationPage";
import {Link, Route, Switch} from "react-router-dom";
import {withRouter} from "react-router-dom";
import {saveAs} from 'file-saver';

import {
    Button,
    ButtonGroup,
    Paper,
    Tab,
    Tabs,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Typography,
    TextField,
} from "@material-ui/core";

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        "aria-controls": `full-width-tabpanel-${index}`,
    };
}

class TicketInfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            commentValue: "",
            tabValue: 0,
            tAttachment: [],
            ticketComments: [],
            ticketHistory: [],
            attachmentId: 0,
            action: "",

            ticketData: {
                id: 42,
                name: "Something",
                date: "2021-07-16",
                category: "Hardware & Software",
                status: "New",
                urgency: "High",
                resolutionDate: "",
                ticketOwner: "Robert Oppenheimer",
                approver: "",
                assignee: "",
                attachment: [],
                description: "Desc",
            },
        };
    }

    componentDidMount() {
        const {ticketId} = this.props.match.params;
        fetch('http://localhost:8080/tickets/' + ticketId, {
            method: 'GET',
            headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then((responseJson) => {
                this.setState({
                    ticketData: {
                        id: responseJson.id,
                        date: responseJson.createdOn,
                        resolutionDate: responseJson.desiredResolutionDate,
                        name: responseJson.name,
                        status: responseJson.state,
                        urgency: responseJson.urgency,
                        ticketOwner: responseJson.owner.firstName,
                        category: responseJson.category.name,
                        description: responseJson.description,
                    }
                })

                this.state.tAttachment = responseJson.attachments;

                if (responseJson.approver != null) {
                    this.state.ticketData.approver = responseJson.approver.firstName
                }

                if (responseJson.assignee != null) {
                    this.state.ticketData.assignee = responseJson.assignee.firstName
                }
            })

        fetch('http://localhost:8080/tickets/' + ticketId + '/histories', {
            method: 'GET',
            headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => response.json())
            .then((responseJson) => this.setState({ticketHistory: responseJson}))

        fetch('http://localhost:8080/tickets/' + ticketId + '/comments', {
            method: 'GET',
            headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => response.json())
            .then((responseJson) => this.setState({ticketComments: responseJson}))

    }

    handleTabChange = (event, value) => {
        this.setState({
            tabValue: value,
        });
    };

    handleEnterComment = (event) => {
        this.setState({
            commentValue: event.target.value,
        });
    };

    addComment = () => {
        fetch('http://localhost:8080/tickets/' + this.state.ticketData.id + '/comments', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem('authorization')
            },
            body: this.state.commentValue
        })
        window.location.reload();
    };

    handleSubmitTicket = () => {

        this.state.action = "SUBMIT"

        this.handleDoAction();
    };

    handleEditTicket = () => {
        console.log("EDIT ticket");
    };

    handleCancelTicket = () => {

        this.state.action = "CANCEL"

        this.handleDoAction();
    };

    handleDoAction = () => {

        fetch('http://localhost:8080/tickets/' + this.state.ticketData.id + '/actions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem('authorization')
            },
            body: "\"" + this.state.action + "\""
        }).then(response => {
            if (response.status === 204) {
                window.location.reload();
            }
        })
    };

    handleAttachment = (id, fileName) => {
        fetch('http://localhost:8080/attachments/' + id,{
            method: 'GET',
            headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => {
                return response.blob()
            })
            .then(blob => {
                saveAs(blob, fileName);
            })
    };

    render() {
        const {
            approver,
            id,
            name,
            date,
            category,
            status,
            urgency,
            resolutionDate,
            ticketOwner,
            assignee,
            attachment,
            description,
        } = this.state.ticketData;

        const {commentValue, tabValue, ticketComments, ticketHistory, tAttachment, attachmentId} =
            this.state;

        const {url} = this.props.match;

        const {handleCancelTicket, handleEditTicket, handleSubmitTicket} = this;

        return (
            <Switch>
                <Route exact path={url}>
                    <div className="ticket-data-container">
                        <div className={"ticket-data-container__back-button back-button"}>
                            <Button component={Link} to="/main-page" variant="contained">
                                Ticket list
                            </Button>
                        </div>
                        <div className="ticket-data-container__title">
                            <Typography variant="h4">{`Ticket(${id}) - ${name}`}</Typography>
                        </div>
                        <div className="ticket-data-container__info">
                            <TableContainer className="ticket-table" component={Paper}>
                                <Table>
                                    <TableBody>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Created on:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {date}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Category:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {category}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Status:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {status}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Urgency:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {urgency}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Desired Resolution Date:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {resolutionDate}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Owner:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {ticketOwner}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Approver:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {approver || "Not assigned"}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Assignee:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {assignee || "Not assigned"}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Attachments:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {tAttachment.map(img =>
                                                        <Button
                                                            key={img.id}
                                                            onClick={() => this.handleAttachment(img.id, img.name)}
                                                        >
                                                            {img.name}
                                                        </Button>
                                                    )}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    Description:
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography align="left" variant="subtitle1">
                                                    {description || "Not assigned"}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                        {status === "DRAFT" && (
                            <div className="ticket-data-container__button-section">
                                <ButtonGroup variant="contained" color="primary">
                                    <Button
                                        onClick={handleSubmitTicket}
                                    >
                                        Submit
                                    </Button>

                                    <Button
                                        component={Link}
                                        to={`/update-ticket/${id}`}
                                        onClick={handleEditTicket}
                                    >
                                        Edit
                                    </Button>
                                    <Button
                                        onClick={handleCancelTicket}
                                    >
                                        Cancel
                                    </Button>
                                </ButtonGroup>
                            </div>
                        )}
                        {status === "DONE" && (
                            <div className="ticket-data-container__button-section">
                                <ButtonGroup variant="contained" color="primary">
                                    <Button
                                        component={Link}
                                        to={`/feedback/${id}`}
                                        onClick={handleEditTicket}
                                    >
                                        Leave Feedback
                                    </Button>
                                    <Button
                                        component={Link}
                                        to={`/feedbackInfo/${id}`}
                                        onClick={handleEditTicket}
                                    >
                                        Feedback
                                    </Button>
                                </ButtonGroup>
                            </div>
                        )}
                        <div className="ticket-data-container__comments-section comments-section">
                            <div className="">
                                <Tabs
                                    variant="fullWidth"
                                    onChange={this.handleTabChange}
                                    value={tabValue}
                                    indicatorColor="primary"
                                    textColor="primary"
                                >
                                    <Tab label="History" {...a11yProps(0)} />
                                    <Tab label="Comments" {...a11yProps(1)} />
                                </Tabs>
                                <TabPanel value={tabValue} index={0}>
                                    <HistoryTable history={ticketHistory}/>
                                </TabPanel>
                                <TabPanel value={tabValue} index={1}>
                                    <CommentsTable comments={ticketComments}/>
                                </TabPanel>
                            </div>
                        </div>
                        {tabValue && (
                            <div className="ticket-data-container__enter-comment-section enter-comment-section">
                                <TextField
                                    label="Enter a comment"
                                    multiline
                                    rows={4}
                                    value={commentValue}
                                    variant="filled"
                                    className="comment-text-field"
                                    onChange={this.handleEnterComment}
                                />
                                <div className="enter-comment-section__add-comment-button">
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={this.addComment}
                                    >
                                        Add Comment
                                    </Button>
                                </div>
                            </div>
                        )}
                    </div>
                </Route>
                <Route path="/create-ticket/:ticketId">
                    <TicketCreationPageWithRouter/>
                </Route>
            </Switch>
        );
    }
}

TicketInfo
    .propTypes = {
    match: PropTypes.object,
};

const
    TicketInfoWithRouter = withRouter(TicketInfo);
export default TicketInfoWithRouter;
