import React from "react";
import {
    Button,
    InputLabel,
    FormControl,
    MenuItem,
    Select,
    TextField,
    Typography,
} from "@material-ui/core";
import {Link, withRouter} from "react-router-dom";
import PropTypes from "prop-types";

class FeedbackPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ticketId: 0,
            ticketName: "",
            rate: 1,
            text: "",
            rateArray: [1, 2, 3, 4, 5],
        }
    }

    componentDidMount() {
        const {ticketId} = this.props.match.params;
        fetch('http://localhost:8080/tickets/' + ticketId, {
            method: 'GET',
            headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then((responseJson) => {
                this.setState({
                    ticketId: responseJson.id,
                    ticketName: responseJson.name,
                })
            })
    }

    handleRateChange = (event) => {
        this.setState({
            rate: event.target.value,
        });
    };

    handleTextChange = (event) => {
        this.setState({
            text: event.target.value,
        });
    };

    handleFeedback = () => {
        fetch('http://localhost:8080/tickets/' + this.state.ticketId + '/feedbacks', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem('authorization')
            },
            body: JSON.stringify({
                rate: this.state.rate,
                text: this.state.text,
            })
        })
    };


    render() {
        const {
            ticketId,
            ticketName,
            rate,
            text,
            rateArray,
        } = this.state;

        return (
            <div className="ticket-creation-form-container">
                <header className="ticket-creation-form-container__navigation-container">
                    <Button component={Link} to="/main-page" variant="contained">
                        Ticket List
                    </Button>
                </header>
                <div className="ticket-data-container__title">
                    <Typography display="block" variant="h4">{`Ticket(${ticketId}) - ${ticketName}`}
                    </Typography>
                </div>
                <div className="ticket-creation-form-container__title">
                    <Typography display="block" variant="h5">
                        Pleas rate your satisfaction with solution
                    </Typography>
                </div>
                <div className="inputs-section-attachment">
                    <div className="inputs-section__ticket-creation-input ticket-creation-input">
                        <FormControl variant="outlined" required>
                            <InputLabel shrink htmlFor="urgency-label">
                                Rate
                            </InputLabel>
                            <Select
                                value={rate}
                                label="Rate"
                                onChange={this.handleRateChange}
                                className={"ticket-creation-input_width200"}
                            >
                                {rateArray.map((item, index) => {
                                    return (
                                        <MenuItem value={item} key={index}>
                                            {item}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    </div>
                </div>

                <div className="inputs-section-attachment">
                    <div className="inputs-section">
                        <FormControl>
                            <TextField
                                label="Comment"
                                multiline
                                rows={4}
                                variant="outlined"
                                value={text}
                                className="creation-text-field creation-text-field_width680"
                                onChange={this.handleTextChange}
                            />
                        </FormControl>
                    </div>
                </div>
                <section className="submit-button-section">
                    <Button
                        component={Link}
                        to="/main-page"
                        variant="contained"
                        onClick={this.handleFeedback}
                        color="primary"
                    >
                        Leave feedback
                    </Button>
                </section>

            </div>
        );
    }
}

FeedbackPage
    .propTypes = {
    match: PropTypes.object,
};

const FeedbackPageWithRouter = withRouter(FeedbackPage);
export default FeedbackPageWithRouter;
