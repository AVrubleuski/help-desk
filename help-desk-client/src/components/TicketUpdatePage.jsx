import React from "react";
import {
    Button,
    InputLabel,
    FormControl,
    MenuItem,
    Select,
    TextField,
    Typography,
} from "@material-ui/core";
import {Link, withRouter} from "react-router-dom";
import {CATEGORIES_OPTIONS, URGENCY_OPTIONS} from "../constants/inputsValues";
import PropTypes from "prop-types";

class TicketUpdatePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            categoryValue: '1',
            nameValue: "",
            descriptionValue: "",
            urgencyValue: "CRITICAL",
            resolutionDateValue: "",
            attachmentValue: "",
            commentValue: "",
            stateValue: "",
            filesValue: [],
        };
    }

    componentDidMount() {

        const {ticketId} = this.props.match.params;
        fetch('http://localhost:8080/tickets/' + ticketId, {
            method: 'GET',
            headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then((responseJson) => {
                this.setState({
                    id: responseJson.id,
                    resolutionDateValue: responseJson.desiredResolutionDate,
                    nameValue: responseJson.name,
                    stateValue: responseJson.state,
                    urgencyValue: responseJson.urgency,
                    categoryValue: responseJson.category.id,
                    descriptionValue: responseJson.description,
                })
            })
    }

    handleCategoryChange = (event) => {
        this.setState({
            categoryValue: event.target.value,
        });
    };

    handleNameChange = (event) => {
        this.setState({
            nameValue: event.target.value,
        });
    };

    handleDescriptionChange = (event) => {
        this.setState({
            descriptionValue: event.target.value,
        });
    };

    handleUrgencyChange = (event) => {
        this.setState({
            urgencyValue: event.target.value,
        });
    };

    handleResolutionDate = (event) => {
        this.setState({
            resolutionDateValue: event.target.value,
        });
    };

    handleAttachmentChange = (event) => {
        this.setState({
            attachmentValue: event.target.value,
        });
    };

    handleCommentChange = (event) => {
        this.setState({
            commentValue: event.target.value,
        });
    };

    handleSaveDraft = () => {
        this.state.stateValue = "DRAFT";
        this.sendMethod()
        console.log("Save as draft");

        window.location.href = '/main-page';
    };

    handleSubmitTicket = () => {

        this.sendMethod()

        fetch('http://localhost:8080/tickets/' + this.state.id + '/actions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem('authorization')
            },
            body: "\"SUBMIT\""
        })

        window.location.href = '/main-page';
    };

    onFileChangeHandler = (e) => {
        for (let i = 0; i < e.target.files.length; i++) {
            this.state.filesValue.push(e.target.files[i])
        }
    };

    sendMethod = () => {
        fetch('http://localhost:8080/tickets/update', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem('authorization')
            },
            body: JSON.stringify({
                id: this.state.id,
                name: this.state.nameValue,
                desiredResolutionDate: this.state.resolutionDateValue,
                urgency: this.state.urgencyValue,
                state: this.state.stateValue,
                description: this.state.descriptionValue,
                category: {
                    id: this.state.categoryValue,
                },
                //textComment: this.state.commentValue
            })
        })
        if (this.state.filesValue.length !== 0) {
            const formData = new FormData();
            for (let i = 0; i < this.state.filesValue.length; i++) {
                formData.append('files', this.state.filesValue[i])
            }
            fetch(
                'http://localhost:8080/tickets/' + this.state.id + '/attachments',
                {
                    method: 'POST',
                    headers: {
                        'Authorization': localStorage.getItem('authorization')
                    },
                    body: formData,
                }
            )
        }

        if (this.state.commentValue !== "") {
            fetch('http://localhost:8080/tickets/' + this.state.id + '/comments', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'Authorization': localStorage.getItem('authorization')
                },
                body: this.state.commentValue
            })
        }
    }

    render() {
        const {
            nameValue,
            attachmentValue,
            categoryValue,
            commentValue,
            descriptionValue,
            resolutionDateValue,
            urgencyValue,
        } = this.state;

        return (
            <div className="ticket-creation-form-container">
                <header className="ticket-creation-form-container__navigation-container">
                    <Button component={Link} to="/main-page" variant="contained">
                        Ticket List
                    </Button>
                </header>
                <div className="ticket-creation-form-container__title">
                    <Typography display="block" variant="h3">
                        Create new ticket
                    </Typography>
                </div>
                <div className="ticket-creation-form-container__form">
                    <div className="inputs-section">
                        <div
                            className="ticket-creation-form-container__inputs-section inputs-section__ticket-creation-input ticket-creation-input ticket-creation-input_width200">
                            <FormControl>
                                <TextField
                                    required
                                    label="Name"
                                    variant="outlined"
                                    onChange={this.handleNameChange}
                                    id="name-label"
                                    value={nameValue}
                                />
                            </FormControl>
                        </div>
                        <div
                            className="inputs-section__ticket-creation-input ticket-creation-input ticket-creation-input_width200">
                            <FormControl variant="outlined" required>
                                <InputLabel shrink htmlFor="category-label">
                                    Category
                                </InputLabel>
                                <Select
                                    value={categoryValue}
                                    label="Category"
                                    onChange={this.handleCategoryChange}
                                    inputProps={{
                                        name: "category",
                                        id: "category-label",
                                    }}
                                >
                                    {CATEGORIES_OPTIONS.map((item, index) => {
                                        return (
                                            <MenuItem value={item.value} key={index}>
                                                {item.label}
                                            </MenuItem>
                                        );
                                    })}
                                </Select>
                            </FormControl>
                        </div>
                        <div className="inputs-section__ticket-creation-input ticket-creation-input">
                            <FormControl variant="outlined" required>
                                <InputLabel shrink htmlFor="urgency-label">
                                    Urgency
                                </InputLabel>
                                <Select
                                    value={urgencyValue}
                                    label="Urgency"
                                    onChange={this.handleUrgencyChange}
                                    className={"ticket-creation-input_width200"}
                                    inputProps={{
                                        name: "urgency",
                                        id: "urgency-label",
                                    }}
                                >
                                    {URGENCY_OPTIONS.map((item, index) => {
                                        return (
                                            <MenuItem value={item.value} key={index}>
                                                {item.label}
                                            </MenuItem>
                                        );
                                    })}
                                </Select>
                            </FormControl>
                        </div>
                    </div>
                    <div className="inputs-section-attachment">
                        <div
                            className="inputs-section__ticket-creation-input ticket-creation-input ticket-creation-input_width200">
                            <FormControl>
                                <InputLabel shrink htmlFor="urgency-label">
                                    Desired resolution date
                                </InputLabel>
                                <TextField
                                    onChange={this.handleResolutionDate}
                                    label="Desired resolution date"
                                    type="date"
                                    id="resolution-date"
                                    value={resolutionDateValue}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </FormControl>
                        </div>
                        <div className="ticket-creation-input">
                            <input type="file" className="form-control" name="file" multiple
                                   onChange={this.onFileChangeHandler}/>
                        </div>
                    </div>

                    <div className="inputs-section">
                        <FormControl>
                            <TextField
                                label="Description"
                                multiline
                                rows={4}
                                variant="outlined"
                                value={descriptionValue}
                                className="creation-text-field creation-text-field_width680"
                                onChange={this.handleDescriptionChange}
                            />
                        </FormControl>
                    </div>
                    <div className="inputs-section">
                        <FormControl>
                            <TextField
                                label="Comment"
                                multiline
                                rows={4}
                                variant="outlined"
                                value={commentValue}
                                className="creation-text-field creation-text-field_width680"
                                onChange={this.handleCommentChange}
                            />
                        </FormControl>
                    </div>
                    <section className="submit-button-section">
                        <Button variant="contained" onClick={this.handleSaveDraft}>
                            Save as Draft
                        </Button>
                        <Button
                            variant="contained"
                            onClick={this.handleSubmitTicket}
                            color="primary"
                        >
                            Submit
                        </Button>
                    </section>
                </div>
            </div>
        );
    }
}

TicketUpdatePage
    .propTypes = {
    match: PropTypes.object,
};

const TicketUpdatePageWithRouter = withRouter(TicketUpdatePage);
export default TicketUpdatePageWithRouter;
