import React from "react";
import PropTypes from "prop-types";
import {
    Button,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
} from "@material-ui/core";
import {Link, Redirect, Switch, Route} from "react-router-dom";
import {withRouter} from "react-router-dom";
import {TICKETS_TABLE_COLUMNS} from "../constants/tablesColumns";
import TicketUpdatePageWithRouter from "./TicketUpdatePage";

class TicketsTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 0,
            tactions: [],
        };
    }

    handleSortFieldTable = (fieldName) => {
        if (fieldName !== "Status" && fieldName !== "Action") {
            this.props.sortField(fieldName)
        }
    };

    handleAction = (action, id) => {
        if (action !== "EDIT") {

            fetch('http://localhost:8080/tickets/' + id + '/actions', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'Authorization': localStorage.getItem('authorization')
                },
                body: "\"" + action + "\""
            }).then((response) => {
                if (response.status === 204) {
                    window.location.reload();
                }
            })
        }
    };

    render() {
        const {searchCallback, tickets, ticketsQuantity, rowsPerPage} = this.props;
        let {page, tactions} = this.state;
        const {url} = this.props.match;
        return (
            <Paper>
                <TableContainer>
                    <TextField
                        onChange={searchCallback}
                        id="filled-full-width"
                        label="Search"
                        style={{margin: 5, width: "500px"}}
                        placeholder="Search for ticket"
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <Table>
                        <TableHead>
                            <TableRow>
                                {TICKETS_TABLE_COLUMNS.map((column) => (
                                    <TableCell align={column.align} key={column.id}>
                                        <b><Button
                                            onClick={() => this.handleSortFieldTable(column.label)}
                                        >
                                            {column.label}
                                        </Button></b>
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tickets
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    return (
                                        <TableRow hover role="checkbox" key={index}>
                                            {TICKETS_TABLE_COLUMNS.map((column) => {
                                                const value = row[column.id];
                                                if (column.id === "name") {
                                                    return (
                                                        <TableCell key={column.id}>
                                                            <Link to={`${url}/${row.id}`}>{value}</Link>
                                                        </TableCell>
                                                    );
                                                }
                                                if (column.id === "actions") {
                                                    tactions = row.actions;
                                                    return (
                                                        <TableCell key={column.id}>
                                                            {tactions.map(action => {
                                                                    if (action === "EDIT") {
                                                                        return (<Button
                                                                            component={Link}
                                                                            to={`/update-ticket/${row.id}`}
                                                                            onClick={this.handleCreate}
                                                                            variant="contained"
                                                                            color="primary"
                                                                        >
                                                                            EDIT
                                                                        </Button>);
                                                                    } else if (action === "SEE_FEEDBACK") {
                                                                        return (<Button
                                                                            component={Link}
                                                                            to={`/feedbackInfo/${row.id}`}
                                                                            onClick={this.handleCreate}
                                                                            variant="contained"
                                                                            color="primary"
                                                                        >
                                                                            {action}
                                                                        </Button>);
                                                                    } else if (action === "LEAVE_FEEDBACK") {
                                                                        return (<Button
                                                                            component={Link}
                                                                            to={`/feedback/${row.id}`}
                                                                            onClick={this.handleCreate}
                                                                            variant="contained"
                                                                            color="primary"
                                                                        >
                                                                            {action}
                                                                        </Button>);
                                                                    } else {
                                                                        return (<Button
                                                                            variant="contained"
                                                                            color="primary"
                                                                            onClick={() => this.handleAction(action, row.id)}
                                                                        >
                                                                            {action}
                                                                        </Button>);
                                                                    }
                                                                }
                                                            )}
                                                        </TableCell>
                                                    );
                                                } else {
                                                    return <TableCell key={column.id}>{value}</TableCell>;
                                                }
                                            })}
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        );
    }
}

TicketsTable.propTypes = {
    searchCallback: PropTypes.func,
    tickets: PropTypes.array,
    ticketsQuantity: PropTypes.number,
    rowsPerPage: PropTypes.number,
    sortField: PropTypes.func,
};

const TicketsTableWithRouter = withRouter(TicketsTable);
export default TicketsTableWithRouter;
