import React from "react";
import {
    Button,
    Typography,
} from "@material-ui/core";
import {Link, withRouter} from "react-router-dom";
import PropTypes from "prop-types";

class FeedbackInfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ticketId: 0,
            ticketName: "",
            rate: 0,
            text: "No feedback for this ticket",
        }
    }

    componentDidMount() {
        const {ticketId} = this.props.match.params;
        fetch('http://localhost:8080/tickets/' + ticketId, {
            method: 'GET',
            headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then((responseJson) => {
                this.setState({
                    ticketId: responseJson.id,
                    ticketName: responseJson.name,
                })
            })

        fetch('http://localhost:8080/tickets/' + ticketId + '/feedbacks',{
            method: 'GET',
                headers: {
                'Authorization': localStorage.getItem('authorization')
            }
        })
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then((responseJson) => {
                if(responseJson!= null){
                    this.setState({
                        rate: responseJson.rate,
                        text: responseJson.text,
                    })
                }
            })
    }

    render() {
        const {
            ticketId,
            ticketName,
            rate,
            text,
        } = this.state;

        return (
            <div className="ticket-creation-form-container">
                <header className="ticket-creation-form-container__navigation-container">
                    <Button component={Link} to="/main-page" variant="contained">
                        Ticket List
                    </Button>
                </header>
                <div className="ticket-data-container__title">
                    <Typography display="block" variant="h5">{`Ticket(${ticketId}) - ${ticketName}`}
                    </Typography>
                </div>
                <div className="inputs-section-attachment">
                    <div className="inputs-section__ticket-creation-input ticket-creation-input">
                        <Typography align="left" variant="subtitle1">
                            Rate: {rate}
                        </Typography>
                    </div>
                </div>
                <div className="inputs-section-attachment">
                    <div className="inputs-section">
                        <Typography align="left" variant="subtitle1">
                            Comment: {text}
                        </Typography>
                    </div>
                </div>
            </div>
        );
    }
}

FeedbackInfo
    .propTypes = {
    match: PropTypes.object,
};

const FeedbackInfoWithRouter = withRouter(FeedbackInfo);
export default FeedbackInfoWithRouter;
