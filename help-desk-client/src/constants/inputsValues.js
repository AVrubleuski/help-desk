export const URGENCY_OPTIONS = [
  { value: "CRITICAL", label: "Critical" },
  { value: "HIGH", label: "High" },
  { value: "AVERAGE", label: "Average" },
  { value: "LOW", label: "Low" },
];

export const CATEGORIES_OPTIONS = [
  { label: "Application & Service", value: 1 },
  { label: "Benefits & Paper Work", value: 2 },
  { label: "Hardware & Software", value: 3 },
  { label: "People Management", value: 4 },
  { label: "Security & Access", value: 5 },
  { label: "Workplaces & Facilities", value: 6 },
];
