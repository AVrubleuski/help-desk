package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.dao.TicketDao;
import com.training.avrubleuski.dto.SearchParamDTO;
import com.training.avrubleuski.dto.TicketsListWrapperDto;
import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.dto.tickets.TicketForCreate;
import com.training.avrubleuski.dto.tickets.TicketForList;
import com.training.avrubleuski.dto.tickets.TicketForView;
import com.training.avrubleuski.entity.Attachment;
import com.training.avrubleuski.entity.Comment;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.TicketsListWrapper;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;
import com.training.avrubleuski.entity.enums.Urgency;
import com.training.avrubleuski.exception.TicketNotFoundException;
import com.training.avrubleuski.service.ActionService;
import com.training.avrubleuski.service.AttachmentService;
import com.training.avrubleuski.service.UserService;
import com.training.avrubleuski.service.mapper.TicketForViewMapper;
import com.training.avrubleuski.service.mapper.TicketMapper;
import com.training.avrubleuski.service.validation.PermissionValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceImplTest {

    private static final Long ONE = 1L;
    private static final Long TWO = 2L;

    private static final Urgency HIGH = Urgency.HIGH;

    private static final String TEST_COMMENT = "TEST_COMMENT";

    private static final Role MANAGER = Role.MANAGER;

    private static final User USER = new User();

    private static final SearchParamDTO SEARCH_PARAM = new SearchParamDTO();

    private static final Ticket TICKET_ONE = new Ticket();
    private static final Ticket TICKET_TWO = new Ticket();

    private static final TicketForList TICKET_FOR_LIST_ONE = new TicketForList();
    private static final TicketForList TICKET_FOR_LIST_TWO = new TicketForList();

    private static final TicketForCreate TICKET_FOR_CREATE = new TicketForCreate();

    private static final TicketForView TICKET_FOR_VIEW = new TicketForView();

    private static final TicketsListWrapper TICKETS_LIST_WRAPPER =
            new TicketsListWrapper(List.of(TICKET_ONE, TICKET_TWO), TWO);

    private static final MultipartFile FILE_ONE = new MockMultipartFile("FILE ONE",
            "FileOne.pdf",
            "text/plain",
            "FILE_ONE".getBytes());

    private static final MultipartFile FILE_TWO = new MockMultipartFile("FILE TWO",
            "FileTwo.pdf",
            "text/plain",
            "FILE_TWO".getBytes());

    private static final Attachment ATTACHMENT_ONE = new Attachment();
    private static final Attachment ATTACHMENT_TWO = new Attachment();

    private static final Action SUBMIT = Action.SUBMIT;

    @Mock
    private TicketDao ticketDao;
    @Mock
    private UserService userService;
    @Mock
    private TicketMapper ticketMapper;
    @Mock
    private TicketForViewMapper ticketForViewMapper;
    @Mock
    private AttachmentService attachmentService;
    @Mock
    private ActionService actionService;
    @Mock
    private PermissionValidator permissionValidator;

    @InjectMocks
    private TicketServiceImpl ticketService;

    @Before
    public void setUp() {
        USER.setId(ONE);
        USER.setRole(MANAGER);

        TICKET_ONE.setId(ONE);
        TICKET_ONE.setUrgency(HIGH);

        ATTACHMENT_ONE.setName("FileOne.pdf");
        ATTACHMENT_ONE.setBlob("FILE_ONE".getBytes());
        ATTACHMENT_TWO.setName("FileTwo.pdf");
        ATTACHMENT_TWO.setBlob("FILE_TWO".getBytes());
    }

    @Test
    public void getTicketsTest() {
        when(userService.getCurrentUser()).thenReturn(USER);
        when(ticketDao.getManagerTickets(ONE, SEARCH_PARAM)).thenReturn(TICKETS_LIST_WRAPPER);
        when(ticketMapper.mapEntitiesToDto(TICKETS_LIST_WRAPPER.getTickets())).thenReturn(List.of(TICKET_FOR_LIST_ONE, TICKET_FOR_LIST_TWO));

        TicketsListWrapperDto expected = new TicketsListWrapperDto(List.of(TICKET_FOR_LIST_ONE, TICKET_FOR_LIST_TWO), TWO);
        TicketsListWrapperDto actual = ticketService.getTickets(SEARCH_PARAM);
        Assert.assertEquals(expected, actual);

        verify(userService, times(1)).getCurrentUser();
        verify(ticketDao, times(1)).getManagerTickets(any(Long.class), any(SearchParamDTO.class));
        verify(ticketMapper, times(1)).mapEntitiesToDto(any(List.class));
        verifyNoMoreInteractions(userService, ticketDao, ticketMapper);
    }

    @Test
    public void getMyTicketsTest() {
        when(userService.getCurrentUser()).thenReturn(USER);
        when(ticketDao.getManagerMyTickets(ONE, SEARCH_PARAM)).thenReturn(TICKETS_LIST_WRAPPER);
        when(ticketMapper.mapEntitiesToDto(TICKETS_LIST_WRAPPER.getTickets())).thenReturn(List.of(TICKET_FOR_LIST_ONE, TICKET_FOR_LIST_TWO));

        TicketsListWrapperDto expected = new TicketsListWrapperDto(List.of(TICKET_FOR_LIST_ONE, TICKET_FOR_LIST_TWO), TWO);
        TicketsListWrapperDto actual = ticketService.getMyTickets(SEARCH_PARAM);
        Assert.assertEquals(expected, actual);

        verify(userService, times(1)).getCurrentUser();
        verify(ticketDao, times(1)).getManagerMyTickets(any(Long.class), any(SearchParamDTO.class));
        verify(ticketMapper, times(1)).mapEntitiesToDto(any(List.class));
        verifyNoMoreInteractions(userService, ticketDao, ticketMapper);
    }

    @Test
    public void saveTicketTest() {
        when(userService.getCurrentUser()).thenReturn(USER);
        doNothing().when(permissionValidator).validateCreateTicket(USER);
        when(ticketMapper.mapDtoToEntity(TICKET_FOR_CREATE)).thenReturn(TICKET_ONE);

        Ticket expected = new Ticket();
        expected.addComment(new Comment(TEST_COMMENT, USER));
        expected.setOwner(USER);
        Ticket actual = ticketService.saveTicket(TICKET_FOR_CREATE);

        Assert.assertEquals(expected.getOwner(), actual.getOwner());

        verify(userService, times(1)).getCurrentUser();
        verify(permissionValidator, times(1)).validateCreateTicket(any(User.class));
        verify(ticketMapper, times(1)).mapDtoToEntity(any(TicketForCreate.class));
        verifyNoMoreInteractions(userService, permissionValidator, ticketMapper);

    }

    @Test
    public void updateTicketTest() {
        when(ticketMapper.mapDtoToEntity(TICKET_FOR_CREATE)).thenReturn(TICKET_ONE);
        when(ticketDao.getEntityById(ONE)).thenReturn(Optional.of(TICKET_ONE));
        when(userService.getCurrentUser()).thenReturn(USER);
        doNothing().when(permissionValidator).validateGetTicket(USER, TICKET_ONE);
        doNothing().when(permissionValidator).validateUpdateTicket(USER, TICKET_ONE);

        Ticket actual = ticketService.updateTicket(TICKET_FOR_CREATE);
        Assert.assertEquals(TICKET_ONE, actual);

        verify(ticketMapper, times(1)).mapDtoToEntity(any(TicketForCreate.class));
        verify(userService, times(2)).getCurrentUser();
        verify(permissionValidator, times(1)).validateGetTicket(any(User.class), any(Ticket.class));
        verify(permissionValidator, times(1)).validateUpdateTicket(any(User.class), any(Ticket.class));
        verify(ticketDao, times(1)).getEntityById(anyLong());
        verifyNoMoreInteractions(userService, permissionValidator, ticketMapper, ticketDao);
    }

    @Test
    public void addAttachmentTest() {
        when(userService.getCurrentUser()).thenReturn(USER);
        doNothing().when(permissionValidator).validateGetTicket(USER, TICKET_ONE);
        when(ticketDao.getEntityById(ONE)).thenReturn(Optional.of(TICKET_ONE));
        when(attachmentService.createAttachment(FILE_ONE)).thenReturn(ATTACHMENT_ONE);
        when(attachmentService.createAttachment(FILE_TWO)).thenReturn(ATTACHMENT_TWO);

        Ticket actual = ticketService.addAttachment(ONE, new MultipartFile[]{FILE_ONE, FILE_TWO});
        Assert.assertEquals(Set.of(ATTACHMENT_ONE, ATTACHMENT_TWO), actual.getAttachments());

        verify(userService, times(1)).getCurrentUser();
        verify(permissionValidator, times(1)).validateGetTicket(any(User.class), any(Ticket.class));
        verify(attachmentService, times(2)).createAttachment(any(MultipartFile.class));
        verify(ticketDao, times(1)).getEntityById(anyLong());
        verifyNoMoreInteractions(userService, permissionValidator, attachmentService, ticketDao);
    }

    @Test
    public void getTicketByIdTest() {
        when(ticketDao.getEntityById(ONE)).thenReturn(Optional.of(TICKET_ONE));
        when(userService.getCurrentUser()).thenReturn(USER);
        doNothing().when(permissionValidator).validateGetTicket(USER, TICKET_ONE);

        Ticket actual = ticketService.getTicketById(ONE);
        Assert.assertEquals(TICKET_ONE, actual);

        verify(userService, times(1)).getCurrentUser();
        verify(permissionValidator, times(1)).validateGetTicket(any(User.class), any(Ticket.class));
        verify(ticketDao, times(1)).getEntityById(anyLong());
        verifyNoMoreInteractions(userService, permissionValidator, ticketDao);
    }

    @Test(expected = TicketNotFoundException.class)
    public void getTicketByIdNoTicket() {
        when(ticketDao.getEntityById(ONE)).thenReturn(Optional.empty());
        ticketService.getTicketById(ONE);
    }

    @Test
    public void getTicketForViewById() {
        when(ticketDao.getEntityById(ONE)).thenReturn(Optional.of(TICKET_ONE));
        when(userService.getCurrentUser()).thenReturn(USER);
        doNothing().when(permissionValidator).validateGetTicket(USER, TICKET_ONE);
        when(ticketForViewMapper.mapEntityToDto(TICKET_ONE)).thenReturn(TICKET_FOR_VIEW);

        TicketForView actual = ticketService.getTicketForViewById(ONE);
        Assert.assertEquals(TICKET_FOR_VIEW, actual);

        verify(userService, times(1)).getCurrentUser();
        verify(permissionValidator, times(1)).validateGetTicket(any(User.class), any(Ticket.class));
        verify(ticketDao, times(1)).getEntityById(anyLong());
        verify(ticketForViewMapper, times(1)).mapEntityToDto(any(Ticket.class));
        verifyNoMoreInteractions(userService, permissionValidator, ticketDao, ticketForViewMapper);
    }

    @Test
    public void updateTicketState() {
        when(ticketDao.getEntityById(ONE)).thenReturn(Optional.of(TICKET_ONE));
        when(userService.getCurrentUser()).thenReturn(USER);
        doNothing().when(permissionValidator).validateGetTicket(USER, TICKET_ONE);
        when(actionService.doAction(SUBMIT, TICKET_ONE, USER)).thenReturn(TICKET_ONE);

        Ticket actual = ticketService.updateTicketState(ONE, SUBMIT);
        Assert.assertEquals(TICKET_ONE, actual);

        verify(userService, times(2)).getCurrentUser();
        verify(permissionValidator, times(1)).validateGetTicket(any(User.class), any(Ticket.class));
        verify(ticketDao, times(1)).getEntityById(anyLong());
        verify(actionService, times(1)).doAction(any(Action.class), any(Ticket.class), any(User.class));
        verifyNoMoreInteractions(userService, permissionValidator, ticketDao, actionService);

    }
}