package com.training.avrubleuski.service.mapper;

import com.training.avrubleuski.config.AppConfig;
import com.training.avrubleuski.dao.FeedbackDao;
import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.dto.tickets.TicketForList;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;
import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.entity.enums.Urgency;
import com.training.avrubleuski.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.*;

@SpringBootTest(classes = {AppConfig.class, ActionMapper.class, TicketMapper.class})
class TicketMapperTest {

    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final String NAME = "NAME";
    private static final Urgency HIGH = Urgency.HIGH;
    private static final State NEW = State.NEW;
    private static final State DONE = State.DONE;
    private static final Role EMPLOYEE = Role.EMPLOYEE;

    private final static User USER = new User();


    private final static Ticket TICKET = new Ticket();
    private final static Ticket TICKET_ONE = new Ticket();
    private final static TicketForList TICKET_FOR_LIST = new TicketForList();

    @MockBean
    private UserService userService;
    @MockBean
    private FeedbackDao feedbackDao;

    @Autowired
    private TicketMapper ticketMapper;

    @BeforeEach
    public void setUp() {
        TICKET_FOR_LIST.setId(ONE);
        TICKET_FOR_LIST.setName(NAME);
        TICKET_FOR_LIST.setUrgency(HIGH);

        USER.setId(ONE);
        USER.setRole(EMPLOYEE);

        TICKET.setId(ONE);
        TICKET.setName(NAME);
        TICKET.setUrgency(HIGH);
        TICKET.setOwner(USER);

        TICKET_ONE.setId(TWO);
        TICKET_ONE.setName(NAME);
        TICKET_ONE.setUrgency(HIGH);
        TICKET_ONE.setOwner(USER);
    }

    @Test
    void testMapDtoToEntitySuccess() {
        Ticket actual = ticketMapper.mapDtoToEntity(TICKET_FOR_LIST);

        Assertions.assertEquals(ONE, actual.getId());
        Assertions.assertEquals(NAME, actual.getName());
        Assertions.assertEquals(HIGH, actual.getUrgency());
    }

    @Test
    void testMapEntityToDtoSuccessStateNew() {
        TICKET.setState(NEW);

        when(userService.getCurrentUser()).thenReturn(USER);

        TicketForList actual = ticketMapper.mapEntityToDto(TICKET);

        Assertions.assertEquals(ONE, actual.getId());
        Assertions.assertEquals(NAME, actual.getName());
        Assertions.assertEquals(HIGH, actual.getUrgency());

        verify(userService, times(1)).getCurrentUser();
        verifyNoMoreInteractions(userService);
    }


    @Test
    void testMapEntityToDtoSuccessStateDone() {
        TICKET.setState(DONE);

        when(userService.getCurrentUser()).thenReturn(USER);
        when(feedbackDao.getEntityById(ONE)).thenReturn(Optional.empty());

        TicketForList actual = ticketMapper.mapEntityToDto(TICKET);

        Assertions.assertEquals(ONE, actual.getId());
        Assertions.assertEquals(NAME, actual.getName());
        Assertions.assertEquals(HIGH, actual.getUrgency());
        Assertions.assertEquals(Set.of(Action.LEAVE_FEEDBACK), actual.getActions());

        verify(userService, times(1)).getCurrentUser();
        verify(feedbackDao, times(1)).getEntityById(anyLong());
        verifyNoMoreInteractions(userService, feedbackDao);
    }

    @Test
    void testMapEntitiesToDtoSuccess() {
        TICKET.setState(DONE);
        TICKET_ONE.setState(NEW);

        when(userService.getCurrentUser()).thenReturn(USER);
        when(feedbackDao.getEntityById(ONE)).thenReturn(Optional.empty());

        List<TicketForList> actual = ticketMapper.mapEntitiesToDto(List.of(TICKET, TICKET_ONE));

        Assertions.assertEquals(TWO, actual.size());

        verify(userService, times(2)).getCurrentUser();
        verify(feedbackDao, times(1)).getEntityById(anyLong());
        verifyNoMoreInteractions(userService, feedbackDao);
    }
}