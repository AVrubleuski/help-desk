package com.training.avrubleuski.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.avrubleuski.dto.CategoryDto;
import com.training.avrubleuski.dto.CommentDto;
import com.training.avrubleuski.dto.FeedbackDto;
import com.training.avrubleuski.dto.HistoryDto;
import com.training.avrubleuski.dto.SearchParamDTO;
import com.training.avrubleuski.dto.TicketsListWrapperDto;
import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.dto.tickets.TicketForCreate;
import com.training.avrubleuski.dto.tickets.TicketForView;
import com.training.avrubleuski.entity.Comment;
import com.training.avrubleuski.entity.Feedback;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.entity.enums.Urgency;
import com.training.avrubleuski.service.CommentService;
import com.training.avrubleuski.service.FeedbackService;
import com.training.avrubleuski.service.HistoryService;
import com.training.avrubleuski.service.TicketService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TicketControllerTest {

    private static final Long ONE = 1L;

    private static final String NAME = "name";
    private static final String NAME_NOT_VALID = "n A mE";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final State NEW = State.NEW;
    private static final Urgency HIGH = Urgency.HIGH;
    private static final LocalDate RESOLUTION_DATE = LocalDate.of(2022, 12, 12);

    private static final TicketsListWrapperDto TICKETS_LIST_WRAPPER_DTO = new TicketsListWrapperDto(new ArrayList<>(), 1L);

    private static final CategoryDto CATEGORY_DTO = new CategoryDto();

    private static final Ticket TICKET_ONE = new Ticket();

    private static final TicketForCreate TICKET_FOR_CREATE = new TicketForCreate();

    private static final TicketForView TICKET_FOR_VIEW = new TicketForView();

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final MockMultipartFile FILE_ONE = new MockMultipartFile("files",
            "FileOne.pdf",
            "text/plain",
            "FILE_ONE".getBytes());

    private static final MockMultipartFile FILE_TWO = new MockMultipartFile("files",
            "FileTwo.pdf",
            "text/plain",
            "FILE_TWO".getBytes());

    private static final CommentDto COMMENT_DTO = new CommentDto();
    private static final Comment COMMENT = new Comment();

    private static final FeedbackDto FEEDBACK_DTO = new FeedbackDto();

    private static final Action SUBMIT = Action.SUBMIT;

    private static final HistoryDto HISTORY_DTO = new HistoryDto();

    @Mock
    private TicketService ticketService;
    @Mock
    private CommentService commentService;
    @Mock
    private FeedbackService feedbackService;
    @Mock
    private HistoryService historyService;

    @InjectMocks
    private TicketController ticketController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(ticketController)
                .setControllerAdvice(new ExceptionController())
                .build();

        MAPPER.findAndRegisterModules();

        TICKET_ONE.setId(ONE);
        TICKET_FOR_CREATE.setCategory(CATEGORY_DTO);
        TICKET_FOR_CREATE.setName(NAME);
        TICKET_FOR_CREATE.setUrgency(HIGH);
        TICKET_FOR_CREATE.setState(NEW);
        TICKET_FOR_CREATE.setDescription(DESCRIPTION);
        TICKET_FOR_CREATE.setDesiredResolutionDate(RESOLUTION_DATE);

        TICKET_FOR_VIEW.setId(ONE);

        COMMENT_DTO.setText("");
        COMMENT.setId(ONE);

        FEEDBACK_DTO.setText(NAME);
    }

    @Test
    public void testGetTickets() throws Exception {

        when(ticketService.getTickets(any(SearchParamDTO.class))).thenReturn(TICKETS_LIST_WRAPPER_DTO);

        this.mockMvc.perform(get("/tickets/pageNumber/pageSize/sortField/sortDirection/filterValue/myTicket")
                .queryParam("pageNumber", "1")
                .queryParam("pageSize", "5")
                .queryParam("sortField", "URGENCY")
                .queryParam("sortDirection", "ASC")
                .queryParam("filterValue", "")
                .queryParam("myTicket", "false"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.tickets").isArray())
                .andExpect(jsonPath("$.ticketsQuantity").value("1"));

        verify(ticketService, times(1)).getTickets(any(SearchParamDTO.class));
        verifyNoMoreInteractions(ticketService);
    }

    @Test
    public void testGetMyTickets() throws Exception {

        when(ticketService.getMyTickets(any(SearchParamDTO.class))).thenReturn(TICKETS_LIST_WRAPPER_DTO);

        this.mockMvc.perform(get("/tickets/pageNumber/pageSize/sortField/sortDirection/filterValue/myTicket")
                .queryParam("pageNumber", "1")
                .queryParam("pageSize", "5")
                .queryParam("sortField", "URGENCY")
                .queryParam("sortDirection", "ASC")
                .queryParam("filterValue", "")
                .queryParam("myTicket", "true"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.tickets").isArray())
                .andExpect(jsonPath("$.ticketsQuantity").value("1"));

        verify(ticketService, times(1)).getMyTickets(any(SearchParamDTO.class));
        verifyNoMoreInteractions(ticketService);
    }

    @Test
    public void testCreateTicketValidInput() throws Exception {
        when(ticketService.saveTicket(TICKET_FOR_CREATE)).thenReturn(TICKET_ONE);

        this.mockMvc.perform(put("/tickets")
                .content(MAPPER.writeValueAsString(TICKET_FOR_CREATE))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().stringValues("Location", "http://localhost/tickets/" + TICKET_ONE.getId()));

        verify(ticketService, times(1)).saveTicket(any(TicketForCreate.class));
        verifyNoMoreInteractions(ticketService);
    }

    @Test
    public void testCreateTicketNoValidInput() throws Exception {

        TICKET_FOR_CREATE.setName(NAME_NOT_VALID);

        this.mockMvc.perform(put("/tickets")
                .content(MAPPER.writeValueAsString(TICKET_FOR_CREATE))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorMessage").value("Illegal symbol in name"));
    }

    @Test
    public void updateTicket() throws Exception {

        when(ticketService.updateTicket(TICKET_FOR_CREATE)).thenReturn(TICKET_ONE);

        this.mockMvc.perform(put("/tickets/update")
                .content(MAPPER.writeValueAsString(TICKET_FOR_CREATE))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(ticketService, times(1)).updateTicket(any(TicketForCreate.class));
        verifyNoMoreInteractions(ticketService);
    }

    @Test
    public void testAddAttachment() throws Exception {

        when(ticketService.addAttachment(any(), any())).thenReturn(TICKET_ONE);

        this.mockMvc.perform(multipart("/tickets/{id}/attachments", ONE)
                .file(FILE_ONE)
                .file(FILE_TWO))
                .andExpect(status().isCreated());

        verify(ticketService, times(1)).addAttachment(anyLong(), any(MultipartFile[].class));
        verifyNoMoreInteractions(ticketService);
    }

    @Test
    public void testGetTicketById() throws Exception {

        when(ticketService.getTicketForViewById(ONE)).thenReturn(TICKET_FOR_VIEW);

        this.mockMvc.perform(get("/tickets/{id}", ONE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1"));

        verify(ticketService, times(1)).getTicketForViewById(anyLong());
        verifyNoMoreInteractions(ticketService);
    }

    @Test
    public void tesGetTicketComments() throws Exception {
        when(commentService.getTicketComments(ONE)).thenReturn(List.of(COMMENT_DTO));

        this.mockMvc.perform(get("/tickets/{id}/comments", ONE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].text").isString());

        verify(commentService, times(1)).getTicketComments(anyLong());
        verifyNoMoreInteractions(commentService);
    }

    @Test
    public void testAddComment() throws Exception {

        when(commentService.saveComment(ONE, NAME)).thenReturn(COMMENT);

        this.mockMvc.perform(post("/tickets/{id}/comments", ONE)
                .content(NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(commentService, times(1)).saveComment(anyLong(), anyString());
        verifyNoMoreInteractions(commentService);
    }

    @Test
    public void getTicketFeedback() throws Exception {
        when(feedbackService.getFeedback(ONE)).thenReturn(FEEDBACK_DTO);

        this.mockMvc.perform(get("/tickets/{id}/feedbacks", ONE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(NAME));

        verify(feedbackService, times(1)).getFeedback(anyLong());
        verifyNoMoreInteractions(feedbackService);
    }

    @Test
    public void testAddFeedback() throws Exception {
        when(feedbackService.saveFeedback(ONE, FEEDBACK_DTO)).thenReturn(new Feedback());

        this.mockMvc.perform(post("/tickets/{id}/feedbacks", ONE)
                .content(MAPPER.writeValueAsString(FEEDBACK_DTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(feedbackService, times(1)).saveFeedback(anyLong(), any(FeedbackDto.class));
        verifyNoMoreInteractions(feedbackService);
    }

    @Test
    public void testDoAction() throws Exception {
        when(ticketService.updateTicketState(ONE, SUBMIT)).thenReturn(TICKET_ONE);

        this.mockMvc.perform(post("/tickets/{id}/actions", ONE)
                .content(MAPPER.writeValueAsString(SUBMIT))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(ticketService, times(1)).updateTicketState(anyLong(), any(Action.class));
        verifyNoMoreInteractions(ticketService);
    }

    @Test
    public void testGetHistories() throws Exception {

        when(historyService.getTicketHistories(ONE)).thenReturn(List.of(HISTORY_DTO));

        this.mockMvc.perform(get("/tickets/{id}/histories", ONE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());

        verify(historyService, times(1)).getTicketHistories(anyLong());
        verifyNoMoreInteractions(historyService);
    }
}