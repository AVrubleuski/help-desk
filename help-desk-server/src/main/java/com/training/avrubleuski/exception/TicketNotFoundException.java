package com.training.avrubleuski.exception;

public class TicketNotFoundException extends DataNotFoundException {

    public TicketNotFoundException(String message) {
        super(message);
    }
}
