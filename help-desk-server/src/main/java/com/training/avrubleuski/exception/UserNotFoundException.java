package com.training.avrubleuski.exception;

public class UserNotFoundException extends DataNotFoundException {

    public UserNotFoundException(String message) {
        super(message);
    }
}