package com.training.avrubleuski.exception;

public class FeedbackNotFoundException extends DataNotFoundException {

    public FeedbackNotFoundException(String message) {
        super(message);
    }
}
