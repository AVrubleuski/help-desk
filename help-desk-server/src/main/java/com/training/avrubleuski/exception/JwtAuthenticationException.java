package com.training.avrubleuski.exception;

public class JwtAuthenticationException extends RuntimeException {

    public JwtAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public JwtAuthenticationException(String message) {
        super(message);
    }
}