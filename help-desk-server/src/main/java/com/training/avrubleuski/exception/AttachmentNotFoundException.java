package com.training.avrubleuski.exception;

public class AttachmentNotFoundException extends DataNotFoundException {

    public AttachmentNotFoundException(String message) {
        super(message);
    }
}
