package com.training.avrubleuski.dto.enums;

public enum SortDirection {
    ASC, DESC
}
