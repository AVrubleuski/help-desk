package com.training.avrubleuski.dto.tickets;

import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.entity.enums.Urgency;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Set;

@Data
public class TicketForList {

    private Long id;

    @NotNull
    @Length(max = 100, message = "Name must be no more than 100 characters")
    @Pattern(regexp = "^[a-z0-9[~.\"(),:;<>@\\[\\]!#$%&'*+\\-/=?^_`{|}]]*$", message = "Illegal symbol in name")
    private String name;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate desiredResolutionDate;

    @NotNull
    private Urgency urgency;

    @NotNull
    private State state;

    private Set<Action> actions;
}
