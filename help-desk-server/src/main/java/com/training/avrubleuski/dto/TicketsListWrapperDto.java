package com.training.avrubleuski.dto;

import com.training.avrubleuski.dto.tickets.TicketForList;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class TicketsListWrapperDto {

    private final List<TicketForList> tickets;

    private final Long ticketsQuantity;

}
