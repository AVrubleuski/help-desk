package com.training.avrubleuski.dto.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum SortField {
    ID("id"),
    NAME("name"),
    DESIRED_RESOLUTION_DATE("desiredResolutionDate"),
    URGENCY("urgency");

    private final String fieldName;
}
