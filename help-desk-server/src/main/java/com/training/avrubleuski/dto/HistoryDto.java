package com.training.avrubleuski.dto;

import lombok.Data;

@Data
public class HistoryDto {

    private String date;

    private String action;

    private String description;

    private UserDto user;
}
