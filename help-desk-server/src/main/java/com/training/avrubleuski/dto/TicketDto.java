package com.training.avrubleuski.dto;

import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.entity.enums.Urgency;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class TicketDto {

    private Long id;

    @NotNull
    @Length(max = 100, message = "Name must be no more than 100 characters")
    @Pattern(regexp = "^[a-z0-9[~.\"(),:;<>@\\[\\]!#$%&'*+\\-/=?^_`{|}]]*$", message = "Illegal symbol in name")
    private String name;

    @NotNull
    @Length(max = 500, message = "description must be no more than 500 characters")
    @Pattern(regexp = "^[a-zA-Z0-9[~.\"(),:;<>@\\[\\]!#$%&'*+\\-/=?^_`{|}]]*$", message = "Illegal symbol in description")
    private String description;

    private String createdOn;

    @NotNull
    private String desiredResolutionDate;

    @NotNull
    private State state;

    @NotNull
    private Urgency urgency;

    private UserDto assignee;

    private UserDto owner;

    private UserDto approver;

    @NotNull
    private CategoryDto category;

    @Length(max = 500, message = "comment must be no more than 500 characters")
    @Pattern(regexp = "^[a-zA-Z0-9[~.\"(),:;<>@\\[\\]!#$%&'*+\\-/=?^_`{|}]]*$", message = "Illegal symbol in comment")
    private String commentText;

    private List<AttachmentDto> attachments;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketDto ticketDto = (TicketDto) o;
        return Objects.equals(id, ticketDto.id) && name.equals(ticketDto.name) && description.equals(ticketDto.description) && createdOn.equals(ticketDto.createdOn) && desiredResolutionDate.equals(ticketDto.desiredResolutionDate) && state == ticketDto.state && urgency == ticketDto.urgency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, createdOn, desiredResolutionDate, state, urgency);
    }

    @Override
    public String toString() {
        return "TicketDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", createdOn=" + createdOn +
                ", desiredResolutionDate=" + desiredResolutionDate +
                ", state=" + state +
                ", urgency=" + urgency +
                '}';
    }
}
