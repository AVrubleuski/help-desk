package com.training.avrubleuski.dto;

import lombok.Data;

@Data
public class ExceptionDescription {

    private String errorMessage;

    private int responseCode;

    private String responseValue;
}
