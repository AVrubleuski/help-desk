package com.training.avrubleuski.dto;

import lombok.Data;

@Data
public class CommentDto {

    private String text;

    private String date;

    private UserDto user;
}
