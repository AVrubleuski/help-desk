package com.training.avrubleuski.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class UserUserCredentialsDto {

    @NotNull
    @Email
    private String email;

    @NotNull
    @Pattern(regexp = "((?=.*[a-z])(?=.*[0-9])(?=.*[~/.\"(),:;<>@\\[\\]!#$% &'*+\\-=?^_`{|}])(?=.*[A-Z]).{6,20})")
    private String password;
}
