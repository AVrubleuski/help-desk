package com.training.avrubleuski.dto;

import com.training.avrubleuski.entity.enums.Role;
import lombok.Data;

@Data
public class UserDto {

    private Long id;

    private String firstName;

    private String lastName;

    private Role role;

    private String email;
}
