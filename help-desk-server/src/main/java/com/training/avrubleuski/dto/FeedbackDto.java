package com.training.avrubleuski.dto;

import lombok.Data;

@Data
public class FeedbackDto {

    private Integer rate;

    private String text;
}
