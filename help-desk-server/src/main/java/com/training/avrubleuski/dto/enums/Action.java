package com.training.avrubleuski.dto.enums;

public enum Action {
    EDIT, SUBMIT, APPROVE, DECLINE, CANCEL, ASSIGN_TO_ME, DONE, SEE_FEEDBACK, LEAVE_FEEDBACK
}
