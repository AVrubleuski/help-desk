package com.training.avrubleuski.dto.tickets;

import com.training.avrubleuski.dto.CategoryDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.ConstraintDeclarationException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = true)
public class TicketForCreate extends TicketForList {

    @NotNull
    @Length(max = 500, message = "description must be no more than 500 characters")
    @Pattern(regexp = "^[a-zA-Z0-9[~/.\"(),:;<>@\\[\\]!#/$% &'*+\\-=?^_`{|}]]*$", message = "Illegal symbol in description")
    private String description;

    @NotNull
    private CategoryDto category;

    @Length(max = 500, message = "comment must be no more than 500 characters")
    @Pattern(regexp = "^[a-zA-Z0-9[~/.\"(),:;<>@\\[\\]!#/$% &'*+\\-=?^_`{|}]]*$", message = "Illegal symbol in comment")
    private String textComment;

    @Override
    public void setDesiredResolutionDate(final LocalDate desiredResolutionDate) {
        if (desiredResolutionDate.isAfter(LocalDate.now())) {
            super.setDesiredResolutionDate(desiredResolutionDate);
        } else {
            throw new ConstraintDeclarationException("Resolution Date not valid");
        }
    }
}
