package com.training.avrubleuski.dto;

import lombok.Data;

@Data
public class AttachmentDto {

    private Long id;

    private String name;
}
