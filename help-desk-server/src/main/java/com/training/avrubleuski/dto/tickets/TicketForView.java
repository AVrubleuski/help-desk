package com.training.avrubleuski.dto.tickets;

import com.training.avrubleuski.dto.AttachmentDto;
import com.training.avrubleuski.dto.CategoryDto;
import com.training.avrubleuski.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class TicketForView extends TicketForList {

    private String createdOn;

    private UserDto assignee;

    private UserDto owner;

    private UserDto approver;

    private Set<AttachmentDto> attachments;

    private String description;

    private CategoryDto category;
}
