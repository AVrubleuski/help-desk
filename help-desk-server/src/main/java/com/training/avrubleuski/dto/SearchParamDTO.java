package com.training.avrubleuski.dto;

import com.training.avrubleuski.dto.enums.SortDirection;
import com.training.avrubleuski.dto.enums.SortField;
import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class SearchParamDTO {

    private Integer pageNumber = 1;

    private Integer pageSize = 5;

    private SortField sortField = SortField.URGENCY;

    private SortDirection sortDirection = SortDirection.ASC;

    @Pattern(regexp = "^[a-zA-Z0-9[~.\"(),:;<>@\\[\\]!#$%&'*+\\-/=?^_`{|}]]*$", message = "Illegal symbol in Description")
    private String filterValue = "";

    private boolean myTicket = false;

    public Integer getFirstResult() {
        return (pageNumber - 1) * pageSize;
    }

}
