package com.training.avrubleuski.util;

import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public final class ResponseEntityUtil {

    private ResponseEntityUtil() {
    }

    public static ResponseEntity<Void> created() {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        return ResponseEntity.created(builder.build().toUri()).build();
    }

    public static ResponseEntity<Void> created(Long id) {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        builder.pathSegment(String.valueOf(id));
        return ResponseEntity.created(builder.build().toUri()).build();
    }


}
