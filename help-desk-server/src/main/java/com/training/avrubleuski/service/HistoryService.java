package com.training.avrubleuski.service;


import com.training.avrubleuski.dto.HistoryDto;
import com.training.avrubleuski.entity.History;

import java.util.List;

public interface HistoryService {

    History saveHistory(History history);

    List<HistoryDto> getTicketHistories(Long ticketId);
}
