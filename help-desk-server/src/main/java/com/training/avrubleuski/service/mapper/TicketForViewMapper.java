package com.training.avrubleuski.service.mapper;

import com.training.avrubleuski.dto.tickets.TicketForView;
import com.training.avrubleuski.entity.Ticket;
import org.springframework.stereotype.Service;

@Service
public class TicketForViewMapper extends AbstractMapper<Ticket, TicketForView> {

    protected TicketForViewMapper() {
        super(Ticket.class, TicketForView.class);
    }
}
