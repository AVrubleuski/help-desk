package com.training.avrubleuski.service;

import com.training.avrubleuski.dto.CommentDto;
import com.training.avrubleuski.entity.Comment;

import java.util.List;

public interface CommentService {

    List<CommentDto> getTicketComments(Long ticketId);

    Comment saveComment(Long ticketId, String commentText);
}
