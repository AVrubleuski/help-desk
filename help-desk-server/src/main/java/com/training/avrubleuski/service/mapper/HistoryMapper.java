package com.training.avrubleuski.service.mapper;

import com.training.avrubleuski.dto.HistoryDto;
import com.training.avrubleuski.entity.History;
import org.springframework.stereotype.Service;

@Service
public class HistoryMapper extends AbstractMapper<History, HistoryDto> {
    protected HistoryMapper() {
        super(History.class, HistoryDto.class);
    }
}
