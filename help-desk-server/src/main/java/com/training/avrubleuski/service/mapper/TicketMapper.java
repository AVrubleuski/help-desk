package com.training.avrubleuski.service.mapper;

import com.training.avrubleuski.dao.FeedbackDao;
import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.dto.tickets.TicketForList;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.service.UserService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TicketMapper extends AbstractMapper<Ticket, TicketForList> {

    private final ActionMapper actionMapper;
    private final UserService userService;
    private final FeedbackDao feedbackDao;

    protected TicketMapper(ActionMapper actionMapper, UserService userService, FeedbackDao feedbackDao) {
        super(Ticket.class, TicketForList.class);
        this.actionMapper = actionMapper;
        this.userService = userService;
        this.feedbackDao = feedbackDao;
    }

    @Override
    public TicketForList mapEntityToDto(Ticket ticket) {
        TicketForList ticketForList = super.mapEntityToDto(ticket);
        ticketForList.setActions(getActionsForTicket(ticket, userService.getCurrentUser()));
        return ticketForList;
    }

    private Set<Action> getActionsForTicket(Ticket ticket, User user) {
        Boolean myTicket = ticket.ownerIdEquals(user.getId());
        return ticket.stateEquals(State.DONE)
                ? actionMapper.getActionsForDone(user.getRole(), myTicket, feedbackDao.getEntityById(ticket.getId()).isPresent())
                : actionMapper.getActions(ticket.getState(), user.getRole(), myTicket);
    }
}
