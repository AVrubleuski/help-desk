package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.dao.TicketDao;
import com.training.avrubleuski.dto.SearchParamDTO;
import com.training.avrubleuski.dto.TicketsListWrapperDto;
import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.dto.tickets.TicketForCreate;
import com.training.avrubleuski.dto.tickets.TicketForView;
import com.training.avrubleuski.entity.Comment;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.TicketsListWrapper;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;
import com.training.avrubleuski.exception.TicketNotFoundException;
import com.training.avrubleuski.service.ActionService;
import com.training.avrubleuski.service.AttachmentService;
import com.training.avrubleuski.service.TicketService;
import com.training.avrubleuski.service.UserService;
import com.training.avrubleuski.service.mapper.TicketForViewMapper;
import com.training.avrubleuski.service.mapper.TicketMapper;
import com.training.avrubleuski.service.validation.PermissionValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.BiFunction;

@Service
public class TicketServiceImpl implements TicketService {

    private final TicketDao ticketDao;
    private final UserService userService;
    private final TicketMapper ticketMapper;
    private final TicketForViewMapper ticketForViewMapper;
    private final AttachmentService attachmentService;
    private final ActionService actionService;
    private final PermissionValidator permissionValidator;

    private final Map<Role, BiFunction<Long, SearchParamDTO, TicketsListWrapper>> ticketsForRole = new EnumMap<>(Role.class);
    private final Map<Role, BiFunction<Long, SearchParamDTO, TicketsListWrapper>> myTicketsForRole = new EnumMap<>(Role.class);

    public TicketServiceImpl(TicketDao ticketDao,
                             UserService userService,
                             TicketMapper ticketMapper,
                             TicketForViewMapper ticketForViewMapper,
                             AttachmentService attachmentService,
                             ActionService actionService,
                             PermissionValidator permissionValidator) {
        this.ticketDao = ticketDao;
        this.userService = userService;
        this.ticketMapper = ticketMapper;
        this.ticketForViewMapper = ticketForViewMapper;
        this.attachmentService = attachmentService;
        this.actionService = actionService;
        this.permissionValidator = permissionValidator;

        ticketsForRole.put(Role.EMPLOYEE, this.ticketDao::getEmployeeTickets);
        ticketsForRole.put(Role.MANAGER, this.ticketDao::getManagerTickets);
        ticketsForRole.put(Role.ENGINEER, this.ticketDao::getEngineerTickets);

        myTicketsForRole.put(Role.EMPLOYEE, this.ticketDao::getEmployeeTickets);
        myTicketsForRole.put(Role.MANAGER, this.ticketDao::getManagerMyTickets);
        myTicketsForRole.put(Role.ENGINEER, this.ticketDao::getEngineerMyTickets);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public TicketsListWrapperDto getTickets(SearchParamDTO parameters) {
        User user = getCurrentUser();
        TicketsListWrapper wrapper = ticketsForRole.get(user.getRole()).apply(user.getId(), parameters);
        return new TicketsListWrapperDto(ticketMapper.mapEntitiesToDto(wrapper.getTickets()), wrapper.getTicketsQuantity());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public TicketsListWrapperDto getMyTickets(SearchParamDTO parameters) {
        User user = getCurrentUser();
        TicketsListWrapper wrapper = myTicketsForRole.get(user.getRole()).apply(user.getId(), parameters);
        return new TicketsListWrapperDto(ticketMapper.mapEntitiesToDto(wrapper.getTickets()), wrapper.getTicketsQuantity());
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Ticket saveTicket(TicketForCreate ticketForCreate) {
        User user = getCurrentUser();
        permissionValidator.validateCreateTicket(user);
        Ticket ticket = ticketMapper.mapDtoToEntity(ticketForCreate);
        ticket.addComment(new Comment(ticketForCreate.getTextComment(), user));
        user.addOwnerTicket(ticket);
        return ticket;
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Ticket updateTicket(TicketForCreate ticketForCreate) {
        Ticket tempTicket = ticketMapper.mapDtoToEntity(ticketForCreate);
        Ticket ticket = getTicketById(tempTicket.getId());
        permissionValidator.validateUpdateTicket(getCurrentUser(), ticket);
        ticket.setName(tempTicket.getName());
        ticket.setDescription(tempTicket.getDescription());
        ticket.setUrgency(tempTicket.getUrgency());
        ticket.setCategory(tempTicket.getCategory());
        ticket.setDesiredResolutionDate(tempTicket.getDesiredResolutionDate());
        return ticket;
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Ticket addAttachment(Long ticketId, MultipartFile[] files) {
        Ticket ticket = getTicketById(ticketId);
        Arrays.stream(files)
                .map(attachmentService::createAttachment)
                .forEach(ticket::addAttachment);
        return ticket;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Ticket getTicketById(Long ticketId) {
        Ticket ticket = ticketDao.getEntityById(ticketId)
                .orElseThrow(() -> new TicketNotFoundException("Ticket not found"));
        permissionValidator.validateGetTicket(getCurrentUser(), ticket);
        return ticket;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public TicketForView getTicketForViewById(Long ticketId) {
        return ticketForViewMapper.mapEntityToDto(getTicketById(ticketId));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Ticket updateTicketState(Long ticketId, Action action) {
        Ticket ticket = getTicketById(ticketId);
        actionService.doAction(action, ticket, getCurrentUser());
        return ticket;
    }

    private User getCurrentUser() {
        return userService.getCurrentUser();
    }
}
