package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.dao.HistoryDao;
import com.training.avrubleuski.dto.HistoryDto;
import com.training.avrubleuski.entity.History;
import com.training.avrubleuski.service.HistoryService;
import com.training.avrubleuski.service.mapper.HistoryMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryServiceImpl implements HistoryService {

    private final HistoryDao historyDao;
    private final HistoryMapper historyMapper;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ, propagation = Propagation.REQUIRES_NEW)
    public History saveHistory(History history) {
        return historyDao.saveEntity(history);
    }

    @Override
    public List<HistoryDto> getTicketHistories(Long ticketId) {
        return historyMapper.mapEntitiesToDto(historyDao.getTicketEntities(ticketId));
    }
}
