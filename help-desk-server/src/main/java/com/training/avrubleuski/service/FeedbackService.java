package com.training.avrubleuski.service;

import com.training.avrubleuski.dto.FeedbackDto;
import com.training.avrubleuski.entity.Feedback;

public interface FeedbackService {

    FeedbackDto getFeedback(Long ticketId);

    Feedback saveFeedback(Long ticketId, FeedbackDto feedbackDto);
}
