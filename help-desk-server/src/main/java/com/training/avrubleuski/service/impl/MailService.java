package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;
import com.training.avrubleuski.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class MailService {

    private final JavaMailSender javaMailSender;
    private final SpringTemplateEngine templateEngine;
    private final UserService userService;

    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String TICKET_ID = "ticketId";

    private static final String TEMPLATE_SUBMIT = "/templateSubmit";
    private static final String TEMPLATE_APPROVE = "/templateApprove";
    private static final String TEMPLATE_DECLINE = "/templateDecline";
    private static final String TEMPLATE_CANCEL_FROM_NEW = "/templateCancelFromNew";
    private static final String TEMPLATE_CANCEL_FROM_APPROVED = "/templateCancelFromApproved";
    private static final String TEMPLATE_DONE = "/templateDone";
    private static final String TEMPLATE_FEEDBACK = "/templateFeedback";

    private static final String MAIL_SUBJECT_SUBMIT = "New ticket for approval";
    private static final String MAIL_SUBJECT_APPROVE = "Ticket was approved";
    private static final String MAIL_SUBJECT_DECLINE = "Ticket was declined";
    private static final String MAIL_SUBJECT_CANCEL = "Ticket was cancelled";
    private static final String MAIL_SUBJECT_DONE = "Ticket was done";
    private static final String MAIL_SUBJECT_FEEDBACK = "Feedback was provided";

    @Transactional(noRollbackFor = {MailException.class})
    public void sendSubmitInfo(Ticket ticket) {

        String[] recipientEmails = userService.getUsersByRole(Role.MANAGER).stream()
                .map(User::getEmail)
                .toArray(String[]::new);

        sendEmail(recipientEmails,
                MAIL_SUBJECT_SUBMIT,
                getHtmlContent(ticket, ticket.getOwner(),
                        TEMPLATE_SUBMIT));
    }

    public void sendApproveInfo(Ticket ticket) {

        List<User> engineers = userService.getUsersByRole(Role.ENGINEER);

        String[] recipientEmails = Stream.concat(Stream.of(ticket.getOwner()), engineers.stream())
                .map(User::getEmail)
                .toArray(String[]::new);

        sendEmail(recipientEmails,
                MAIL_SUBJECT_APPROVE,
                getHtmlContent(ticket, ticket.getOwner(),
                        TEMPLATE_APPROVE));
    }

    public void sendDeclineInfo(Ticket ticket) {

        sendEmail(new String[]{ticket.getOwnerEmail()},
                MAIL_SUBJECT_DECLINE,
                getHtmlContent(ticket, ticket.getOwner(),
                        TEMPLATE_DECLINE));
    }

    public void sendCancelFromNewInfo(Ticket ticket) {

        sendEmail(new String[]{ticket.getOwnerEmail()},
                MAIL_SUBJECT_CANCEL,
                getHtmlContent(ticket, ticket.getOwner(),
                        TEMPLATE_CANCEL_FROM_NEW));
    }

    public void sendCancelFromApprovedInfo(Ticket ticket) {

        sendEmail(new String[]{ticket.getOwnerEmail(), ticket.getApproverEmail()},
                MAIL_SUBJECT_CANCEL,
                getHtmlContent(ticket, ticket.getOwner(),
                        TEMPLATE_CANCEL_FROM_APPROVED));
    }

    public void sendDoneInfo(Ticket ticket) {

        sendEmail(new String[]{ticket.getOwnerEmail()},
                MAIL_SUBJECT_DONE,
                getHtmlContent(ticket, ticket.getOwner(),
                        TEMPLATE_DONE));
    }

    public void sendFeedbackInfo(Ticket ticket) {

        sendEmail(new String[]{ticket.getAssigneeEmail()},
                MAIL_SUBJECT_FEEDBACK,
                getHtmlContent(ticket, ticket.getAssignee(),
                        TEMPLATE_FEEDBACK));
    }

    private String getHtmlContent(Ticket ticket, User user, String template) {
        Context ctx = new Context();
        ctx.setVariable(FIRST_NAME, user.getFirstName());
        ctx.setVariable(LAST_NAME, user.getLastName());
        ctx.setVariable(TICKET_ID, ticket.getId());
        return templateEngine.process(template, ctx);
    }

    private void sendEmail(String[] recipientEmails, String subjectText, String htmlContent) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");

            message.setTo(recipientEmails);
            message.setSubject(subjectText);

            message.setText(htmlContent, true);

            javaMailSender.send(mimeMessage);
        } catch (MessagingException | MailException exception) {
            log.error("Send mail fail", exception);
        }
    }
}
