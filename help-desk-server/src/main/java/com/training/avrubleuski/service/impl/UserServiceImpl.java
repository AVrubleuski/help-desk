package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.dao.UserDao;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;
import com.training.avrubleuski.exception.UserNotFoundException;
import com.training.avrubleuski.security.CustomUserDetails;
import com.training.avrubleuski.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public User getCurrentUser() {
        CustomUserDetails customUserDetails =
                (CustomUserDetails) SecurityContextHolder.getContext()
                        .getAuthentication()
                        .getPrincipal();
        return userDao.getEntityById(customUserDetails.getUser().getId())
                .orElseThrow(() -> new UserNotFoundException("UserNotFound with"));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<User> getUsersByRole(Role role) {
        return userDao.getUsersByRole(role);
    }
}
