package com.training.avrubleuski.service;

import com.training.avrubleuski.dto.SearchParamDTO;
import com.training.avrubleuski.dto.TicketsListWrapperDto;
import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.dto.tickets.TicketForCreate;
import com.training.avrubleuski.dto.tickets.TicketForView;
import com.training.avrubleuski.entity.Ticket;
import org.springframework.web.multipart.MultipartFile;

public interface TicketService {

    TicketsListWrapperDto getTickets(SearchParamDTO parameters);

    TicketsListWrapperDto getMyTickets(SearchParamDTO parameters);

    Ticket saveTicket(TicketForCreate ticketForCreate);

    Ticket updateTicket(TicketForCreate ticketForCreate);

    Ticket addAttachment(Long ticketId, MultipartFile[] files);

    Ticket getTicketById(Long ticketId);

    TicketForView getTicketForViewById(Long ticketId);

    Ticket updateTicketState(Long ticketId, Action action);
}
