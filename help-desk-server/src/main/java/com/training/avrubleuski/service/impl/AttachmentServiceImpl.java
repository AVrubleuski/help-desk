package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.dao.AttachmentDao;
import com.training.avrubleuski.entity.Attachment;
import com.training.avrubleuski.exception.AttachmentNotFoundException;
import com.training.avrubleuski.service.AttachmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AttachmentServiceImpl implements AttachmentService {


    private final AttachmentDao attachmentDao;

    @Override
    public Attachment createAttachment(MultipartFile file) {
        Attachment attachment = new Attachment();
        try {
            attachment.setName(file.getOriginalFilename());
            attachment.setBlob(file.getBytes());
        } catch (IOException exception) {
            log.error("Save file fail", exception);
        }
        return attachment;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<Attachment> getTicketAttachments(Long ticketId) {
        return attachmentDao.getTicketEntities(ticketId);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Attachment getAttachmentById(Long attachmentId) {
        return attachmentDao.getEntityById(attachmentId)
                .orElseThrow(() -> new AttachmentNotFoundException("Attachment not found"));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void deleteAttachment(Long attachmentId) {
        Attachment attachment = getAttachmentById(attachmentId);
        attachmentDao.deleteEntity(attachment);
    }
}
