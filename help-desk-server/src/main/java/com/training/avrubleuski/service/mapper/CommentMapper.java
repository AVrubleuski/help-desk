package com.training.avrubleuski.service.mapper;

import com.training.avrubleuski.dto.CommentDto;
import com.training.avrubleuski.entity.Comment;
import org.springframework.stereotype.Service;

@Service
public class CommentMapper extends AbstractMapper<Comment, CommentDto> {

    protected CommentMapper() {
        super(Comment.class, CommentDto.class);
    }
}
