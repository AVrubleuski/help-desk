package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.exception.IllegalActionException;
import com.training.avrubleuski.service.ActionService;
import com.training.avrubleuski.service.mapper.ActionMapper;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.BiFunction;

@Service
public class ActionServiceImpl implements ActionService {

    private final ActionMapper actionMapper;
    private final MailService mailService;

    private final Map<Action, BiFunction<Ticket, User, Ticket>> actionManager = new EnumMap<>(Action.class);

    public ActionServiceImpl(ActionMapper actionMapper, MailService mailService) {
        this.actionMapper = actionMapper;
        this.mailService = mailService;

        actionManager.put(Action.SUBMIT, this::submit);
        actionManager.put(Action.APPROVE, this::approve);
        actionManager.put(Action.DECLINE, this::decline);
        actionManager.put(Action.CANCEL, this::cancel);
        actionManager.put(Action.ASSIGN_TO_ME, this::assign);
        actionManager.put(Action.DONE, this::done);
    }

    private Ticket submit(Ticket ticket, User user) {
        ticket.setState(State.NEW);
        mailService.sendSubmitInfo(ticket);
        return ticket;
    }

    private Ticket approve(Ticket ticket, User user) {
        ticket.setState(State.APPROVED);
        ticket.setApprover(user);
        mailService.sendApproveInfo(ticket);
        return ticket;
    }

    private Ticket decline(Ticket ticket, User user) {
        ticket.setState(State.DECLINED);
        ticket.setApprover(user);
        mailService.sendDeclineInfo(ticket);
        return ticket;
    }

    private Ticket cancel(Ticket ticket, User user) {
        State previousState = ticket.getState();
        ticket.setState(State.CANCELED);
        if (previousState.equals(State.NEW)) {
            ticket.setApprover(user);
            mailService.sendCancelFromNewInfo(ticket);
        }
        if (previousState.equals(State.APPROVED)) {
            ticket.setAssignee(user);
            mailService.sendCancelFromApprovedInfo(ticket);
        }
        return ticket;
    }

    private Ticket assign(Ticket ticket, User user) {
        ticket.setState(State.IN_PROGRESS);
        ticket.setAssignee(user);
        return ticket;
    }

    private Ticket done(Ticket ticket, User user) {
        ticket.setState(State.DONE);
        mailService.sendDoneInfo(ticket);
        return ticket;
    }

    @Override
    public Ticket doAction(Action action, Ticket ticket, User user) {
        if (actionMapper.getActions(ticket.getState(),
                user.getRole(), ticket.ownerIdEquals(user.getId())).contains(action)) {
            return actionManager.get(action).apply(ticket, user);
        }
        throw new IllegalActionException("Operation is not available");
    }
}
