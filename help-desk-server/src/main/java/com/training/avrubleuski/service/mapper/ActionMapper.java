package com.training.avrubleuski.service.mapper;

import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.entity.enums.Role;
import com.training.avrubleuski.entity.enums.State;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.training.avrubleuski.dto.enums.Action.APPROVE;
import static com.training.avrubleuski.dto.enums.Action.ASSIGN_TO_ME;
import static com.training.avrubleuski.dto.enums.Action.CANCEL;
import static com.training.avrubleuski.dto.enums.Action.DECLINE;
import static com.training.avrubleuski.dto.enums.Action.EDIT;
import static com.training.avrubleuski.dto.enums.Action.LEAVE_FEEDBACK;
import static com.training.avrubleuski.dto.enums.Action.SEE_FEEDBACK;
import static com.training.avrubleuski.dto.enums.Action.SUBMIT;
import static com.training.avrubleuski.entity.enums.Role.EMPLOYEE;
import static com.training.avrubleuski.entity.enums.Role.ENGINEER;
import static com.training.avrubleuski.entity.enums.Role.MANAGER;
import static com.training.avrubleuski.entity.enums.State.APPROVED;
import static com.training.avrubleuski.entity.enums.State.DECLINED;
import static com.training.avrubleuski.entity.enums.State.DRAFT;
import static com.training.avrubleuski.entity.enums.State.IN_PROGRESS;
import static com.training.avrubleuski.entity.enums.State.NEW;

@Component
public class ActionMapper {

    private final Map<State, Map<Role, Map<Boolean, Set<Action>>>> availableActions = new EnumMap<>(State.class);
    private final Map<Role, Map<Boolean, Map<Boolean, Set<Action>>>> availableActionsForDONE = new EnumMap<>(Role.class);

    public ActionMapper() {

        Map<Role, Map<Boolean, Set<Action>>> actionsForDraft = new EnumMap<>(Role.class);
        actionsForDraft.put(EMPLOYEE, Map.of(true, Set.of(EDIT, SUBMIT, CANCEL)));
        actionsForDraft.put(MANAGER, Map.of(true, Set.of(EDIT, SUBMIT, CANCEL)));

        Map<Role, Map<Boolean, Set<Action>>> actionsForNew = new EnumMap<>(Role.class);
        actionsForNew.put(MANAGER, Map.of(false, Set.of(APPROVE, DECLINE, CANCEL)));

        Map<Role, Map<Boolean, Set<Action>>> actionsForApproved = new EnumMap<>(Role.class);
        actionsForApproved.put(ENGINEER, Map.of(false, Set.of(ASSIGN_TO_ME, CANCEL)));

        Map<Role, Map<Boolean, Set<Action>>> actionsForDeclined = new EnumMap<>(Role.class);
        actionsForDeclined.put(EMPLOYEE, Map.of(true, Set.of(SUBMIT, CANCEL)));
        actionsForDeclined.put(MANAGER, Map.of(true, Set.of(SUBMIT, CANCEL)));

        Map<Role, Map<Boolean, Set<Action>>> actionsForInProgress = new EnumMap<>(Role.class);
        actionsForInProgress.put(ENGINEER, Map.of(false, Set.of(Action.DONE)));

        availableActions.put(DRAFT, actionsForDraft);
        availableActions.put(NEW, actionsForNew);
        availableActions.put(APPROVED, actionsForApproved);
        availableActions.put(DECLINED, actionsForDeclined);
        availableActions.put(IN_PROGRESS, actionsForInProgress);

        availableActionsForDONE.put(EMPLOYEE, Map.of(true, Map.of(false, Set.of(LEAVE_FEEDBACK),
                true, Set.of(SEE_FEEDBACK))));
        availableActionsForDONE.put(MANAGER, Map.of(true, Map.of(false, Set.of(LEAVE_FEEDBACK),
                true, Set.of(SEE_FEEDBACK)), false, Map.of(true, Set.of(SEE_FEEDBACK))));
        availableActionsForDONE.put(ENGINEER, Map.of(false, Map.of(true, Set.of(SEE_FEEDBACK))));
    }

    public Set<Action> getActions(State state, Role role, Boolean myTicket) {
        return Optional.ofNullable(availableActions.get(state))
                .map(roleMapMap -> roleMapMap.get(role))
                .map(booleanSetMap -> booleanSetMap.get(myTicket))
                .orElse(new HashSet<>());
    }

    public Set<Action> getActionsForDone(Role role, Boolean myTicket, Boolean feedback) {
        return Optional.ofNullable(availableActionsForDONE.get(role))
                .map(booleanMapMap -> booleanMapMap.get(myTicket))
                .map(booleanSetMap -> booleanSetMap.get(feedback))
                .orElse(new HashSet<>());
    }

}
