package com.training.avrubleuski.service.validation;

import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;
import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.exception.IllegalActionException;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;

import static com.training.avrubleuski.entity.enums.Role.EMPLOYEE;
import static com.training.avrubleuski.entity.enums.Role.ENGINEER;
import static com.training.avrubleuski.entity.enums.Role.MANAGER;
import static com.training.avrubleuski.entity.enums.State.APPROVED;
import static com.training.avrubleuski.entity.enums.State.CANCELED;
import static com.training.avrubleuski.entity.enums.State.DECLINED;
import static com.training.avrubleuski.entity.enums.State.DONE;
import static com.training.avrubleuski.entity.enums.State.DRAFT;
import static com.training.avrubleuski.entity.enums.State.IN_PROGRESS;
import static com.training.avrubleuski.entity.enums.State.NEW;

@Service
public class PermissionValidator {

    private static final String MESSAGE = "Operation is not available";
    private static final Set<State> STATES_MANAGER = Set.of(APPROVED, DECLINED, CANCELED, IN_PROGRESS, DONE);
    private static final Set<State> STATES_ENGINEER = Set.of(IN_PROGRESS, DONE);

    private static final Map<Role, List<BiPredicate<User, Ticket>>> CONDITIONS_PERMISSION_FOR_ROLE = new EnumMap<>(Role.class);

    public PermissionValidator() {
        CONDITIONS_PERMISSION_FOR_ROLE.put(EMPLOYEE, List.of(this::isUserOwnerTicket));
        CONDITIONS_PERMISSION_FOR_ROLE.put(MANAGER, List.of(this::isUserOwnerTicket,
                this::checkTicketOwnerRoleAndState,
                this::isUserTicketApprover));
        CONDITIONS_PERMISSION_FOR_ROLE.put(ENGINEER, List.of(this::isTicketStateApprove, this::isUserAssigneeTicket));
    }

    public void validateCreateTicket(User user) {
        if (!Set.of(EMPLOYEE, MANAGER).contains(user.getRole())) {
            throw new IllegalActionException(MESSAGE);
        }
    }

    public void validateUpdateTicket(User user, Ticket ticket) {
        if (!ticket.stateEquals(DRAFT) || !isUserOwnerTicket(user, ticket)) {
            throw new IllegalActionException(MESSAGE);
        }
    }

    public void validateLeaveFeedback(User user, Ticket ticket) {
        if (!ticket.stateEquals(DONE) || !isUserOwnerTicket(user, ticket)) {
            throw new IllegalActionException(MESSAGE);
        }
    }

    public void validateGetTicket(User user, Ticket ticket) {

        CONDITIONS_PERMISSION_FOR_ROLE.get(user.getRole()).stream()
                .filter(biPredicate -> biPredicate.test(user, ticket))
                .findFirst()
                .orElseThrow(() -> new IllegalActionException(MESSAGE));
    }

    private boolean isUserOwnerTicket(User user, Ticket ticket) {
        return ticket.ownerIdEquals(user.getId());
    }

    private boolean checkTicketOwnerRoleAndState(User user, Ticket ticket) {
        return ticket.getState().equals(NEW) && ticket.getOwner().getRole().equals(EMPLOYEE);
    }

    private boolean isUserTicketApprover(User user, Ticket ticket) {
        return ticket.approverIdEquals(user.getId()) && STATES_MANAGER.contains(ticket.getState());
    }


    private boolean isTicketStateApprove(User user, Ticket ticket) {
        return ticket.stateEquals(APPROVED);
    }

    private boolean isUserAssigneeTicket(User user, Ticket ticket) {
        return ticket.assigneeIdEquals(user.getId()) && STATES_ENGINEER.contains(ticket.getState());
    }
}
