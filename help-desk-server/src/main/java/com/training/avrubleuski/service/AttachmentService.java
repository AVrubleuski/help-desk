package com.training.avrubleuski.service;

import com.training.avrubleuski.entity.Attachment;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AttachmentService {

    Attachment createAttachment(MultipartFile file);

    List<Attachment> getTicketAttachments(Long ticketId);

    Attachment getAttachmentById(Long attachmentId);

    void deleteAttachment(Long attachmentId);
}
