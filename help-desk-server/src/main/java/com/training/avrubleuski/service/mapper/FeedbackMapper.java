package com.training.avrubleuski.service.mapper;

import com.training.avrubleuski.dto.FeedbackDto;
import com.training.avrubleuski.entity.Feedback;
import org.springframework.stereotype.Service;

@Service
public class FeedbackMapper extends AbstractMapper<Feedback, FeedbackDto> {

    protected FeedbackMapper() {
        super(Feedback.class, FeedbackDto.class);
    }
}
