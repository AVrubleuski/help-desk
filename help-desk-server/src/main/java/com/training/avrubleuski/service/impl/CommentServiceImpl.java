package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.dao.CommentDao;
import com.training.avrubleuski.dto.CommentDto;
import com.training.avrubleuski.entity.Comment;
import com.training.avrubleuski.service.CommentService;
import com.training.avrubleuski.service.TicketService;
import com.training.avrubleuski.service.UserService;
import com.training.avrubleuski.service.mapper.CommentMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentDao commentDao;
    private final CommentMapper commentMapper;
    private final TicketService ticketService;
    private final UserService userService;

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<CommentDto> getTicketComments(Long ticketId) {
        return commentMapper.mapEntitiesToDto(commentDao.getTicketEntities(ticketId));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Comment saveComment(Long ticketId, String commentText) {
        Comment comment = new Comment(commentText,
                userService.getCurrentUser(),
                ticketService.getTicketById(ticketId));
        return commentDao.saveEntity(comment);
    }
}
