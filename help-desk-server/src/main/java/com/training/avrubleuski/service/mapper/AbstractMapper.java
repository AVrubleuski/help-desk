package com.training.avrubleuski.service.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

abstract class AbstractMapper<E, D> {

    @Autowired
    private ModelMapper modelMapper;

    private final Class<E> entityClass;
    private final Class<D> dtoClass;

    protected AbstractMapper(Class<E> entityClass, Class<D> dtoClass) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }

    public D mapEntityToDto(E entity) {
        return modelMapper.map(entity, dtoClass);
    }

    public E mapDtoToEntity(D dto) {
        return modelMapper.map(dto, entityClass);
    }

    public List<D> mapEntitiesToDto(List<E> entities) {
        return entities.stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toList());
    }
}
