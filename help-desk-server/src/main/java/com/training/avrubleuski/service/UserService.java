package com.training.avrubleuski.service;

import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;

import java.util.List;

public interface UserService {

    User getCurrentUser();

    List<User> getUsersByRole(Role role);
}
