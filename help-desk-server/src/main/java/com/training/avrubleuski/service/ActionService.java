package com.training.avrubleuski.service;

import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.User;

public interface ActionService {

    Ticket doAction(Action action, Ticket ticket, User user);
}
