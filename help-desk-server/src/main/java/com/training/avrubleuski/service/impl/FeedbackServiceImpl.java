package com.training.avrubleuski.service.impl;

import com.training.avrubleuski.dao.FeedbackDao;
import com.training.avrubleuski.dto.FeedbackDto;
import com.training.avrubleuski.entity.Feedback;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.exception.FeedbackNotFoundException;
import com.training.avrubleuski.service.FeedbackService;
import com.training.avrubleuski.service.TicketService;
import com.training.avrubleuski.service.UserService;
import com.training.avrubleuski.service.mapper.FeedbackMapper;
import com.training.avrubleuski.service.validation.PermissionValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class FeedbackServiceImpl implements FeedbackService {

    private final UserService userService;
    private final TicketService ticketService;
    private final FeedbackDao feedbackDao;
    private final FeedbackMapper feedbackMapper;
    private final MailService mailService;
    private final PermissionValidator permissionValidator;

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public FeedbackDto getFeedback(Long ticketId) {
        return feedbackMapper.mapEntityToDto(feedbackDao.getEntityById(ticketId)
                .orElseThrow(() -> new FeedbackNotFoundException("Feedback not found")));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Feedback saveFeedback(Long ticketId, FeedbackDto feedbackDto) {
        Ticket ticket = ticketService.getTicketById(ticketId);
        User user = userService.getCurrentUser();
        permissionValidator.validateLeaveFeedback(user, ticket);
        Feedback feedback = feedbackMapper.mapDtoToEntity(feedbackDto);
        feedback.setUser(user);
        feedback.setTicket(ticket);
        feedbackDao.saveEntity(feedback);
        mailService.sendFeedbackInfo(ticket);
        return feedback;
    }
}
