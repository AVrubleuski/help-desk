package com.training.avrubleuski.listener;

import com.training.avrubleuski.entity.History;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.enums.HistoryAction;
import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.service.impl.HistoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PostLoad;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;

@Component
public class TicketListener {

    private static HistoryServiceImpl historyService;
    private State previousState;

    @Autowired
    public void setHistoryService(HistoryServiceImpl historyService) {
        this.historyService = historyService;
    }

    @PrePersist
    private void prePersist(Ticket ticket) {
        History history = new History(HistoryAction.CREATED.getActionDescription(),
                HistoryAction.CREATED.getActionDescription());
        history.setUser(ticket.getOwner());
        ticket.addHistory(history);
    }

    @PostLoad
    private void storeState(Ticket ticket) {
        previousState = ticket.getState();
    }

    @PostUpdate
    private void preUpdate(Ticket ticket) {
        History history;
        if (ticket.getState().equals(previousState)) {
            history = new History(HistoryAction.EDITED.getActionDescription(),
                    HistoryAction.EDITED.getActionDescription());
        } else {
            history = new History(HistoryAction.CHANGED.getActionDescription(),
                    String.format("%s from %s to %s", HistoryAction.CHANGED.getActionDescription(), previousState, ticket.getState()));
        }

        history.setUser(ticket.getOwner());
        history.setTicket(ticket);
        historyService.saveHistory(history);
    }
}
