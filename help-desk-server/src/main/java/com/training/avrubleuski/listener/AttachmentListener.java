package com.training.avrubleuski.listener;

import com.training.avrubleuski.entity.Attachment;
import com.training.avrubleuski.entity.History;
import com.training.avrubleuski.entity.enums.HistoryAction;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;

public class AttachmentListener {

    @PrePersist
    private void prePersist(Attachment attachment) {
        attachment.addHistory(getHistory(attachment, HistoryAction.ATTACHED));
    }

    @PreRemove
    private void preRemove(Attachment attachment) {
        attachment.addHistory(getHistory(attachment, HistoryAction.REMOVED));
    }

    private History getHistory(Attachment attachment, HistoryAction action) {
        History history = new History(action.getActionDescription(),
                action.getActionDescription() + attachment.getName());
        history.setUser(attachment.getTicket().getOwner());
        return history;
    }
}
