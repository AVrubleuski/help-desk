package com.training.avrubleuski.controller;

import com.training.avrubleuski.dto.CommentDto;
import com.training.avrubleuski.dto.FeedbackDto;
import com.training.avrubleuski.dto.HistoryDto;
import com.training.avrubleuski.dto.SearchParamDTO;
import com.training.avrubleuski.dto.TicketsListWrapperDto;
import com.training.avrubleuski.dto.enums.Action;
import com.training.avrubleuski.dto.tickets.TicketForCreate;
import com.training.avrubleuski.dto.tickets.TicketForView;
import com.training.avrubleuski.service.CommentService;
import com.training.avrubleuski.service.FeedbackService;
import com.training.avrubleuski.service.HistoryService;
import com.training.avrubleuski.service.TicketService;
import com.training.avrubleuski.util.ResponseEntityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;
    private final CommentService commentService;
    private final FeedbackService feedbackService;
    private final HistoryService historyService;

    @GetMapping("/{pageNumber}/{pageSize}/{sortField}/{sortDirection}/{filterValue}/{myTicket}")
    public ResponseEntity<TicketsListWrapperDto> getTickets(@Valid SearchParamDTO parameters) {
        return parameters.isMyTicket()
                ? ResponseEntity.ok(ticketService.getMyTickets(parameters))
                : ResponseEntity.ok(ticketService.getTickets(parameters));
    }

    @PutMapping()
    public ResponseEntity<Void> createTicket(@Valid @RequestBody TicketForCreate ticketForCreate) {
        Long id = ticketService.saveTicket(ticketForCreate).getId();
        return ResponseEntityUtil.created(id);
    }

    @PutMapping("/update")
    public ResponseEntity<Void> updateTicket(@Valid @RequestBody TicketForCreate ticketForCreate) {
        ticketService.updateTicket(ticketForCreate);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{id}/attachments")
    public ResponseEntity<Void> addAttachment(@PathVariable Long id,
                                              @RequestParam("files") MultipartFile[] files) {
        ticketService.addAttachment(id, files);
        return ResponseEntityUtil.created();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TicketForView> getTicketById(@PathVariable Long id) {
        return ResponseEntity.ok(ticketService.getTicketForViewById(id));
    }

    @GetMapping("/{id}/comments")
    public ResponseEntity<List<CommentDto>> getTicketComments(@PathVariable Long id) {
        return ResponseEntity.ok(commentService.getTicketComments(id));
    }

    @PostMapping("/{id}/comments")
    public ResponseEntity<Void> addComment(@PathVariable Long id,
                                           @RequestBody String commentText) {
        Long commentId = commentService.saveComment(id, commentText).getId();
        return ResponseEntityUtil.created(commentId);
    }

    @GetMapping("/{id}/feedbacks")
    public ResponseEntity<FeedbackDto> getTicketFeedback(@PathVariable Long id) {
        return ResponseEntity.ok(feedbackService.getFeedback(id));
    }

    @PostMapping("/{id}/feedbacks")
    public ResponseEntity<Void> addFeedback(@PathVariable Long id,
                                            @RequestBody FeedbackDto feedbackDto) {
        feedbackService.saveFeedback(id, feedbackDto);
        return ResponseEntityUtil.created();
    }

    @PostMapping("/{id}/actions")
    public ResponseEntity<Void> doAction(@PathVariable Long id,
                                         @RequestBody Action action) {
        ticketService.updateTicketState(id, action);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}/histories")
    public ResponseEntity<List<HistoryDto>> getHistories(@PathVariable Long id) {
        return ResponseEntity.ok(historyService.getTicketHistories(id));
    }
}
