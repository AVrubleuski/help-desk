package com.training.avrubleuski.controller;

import com.training.avrubleuski.dto.UserUserCredentialsDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @ApiOperation("Use it to get the jwt token")
    @PostMapping("")
    public void fakeLogin(@ApiParam("LogUser") @RequestBody UserUserCredentialsDto logUser) {
        throw new IllegalStateException("This method shouldn't be called. It's implemented by Spring Security filters.");
    }
}
