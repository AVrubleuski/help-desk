package com.training.avrubleuski.controller;

import com.training.avrubleuski.dto.ExceptionDescription;
import com.training.avrubleuski.exception.DataNotFoundException;
import com.training.avrubleuski.exception.IllegalActionException;
import com.training.avrubleuski.exception.JwtAuthenticationException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<ExceptionDescription> handleException(DataNotFoundException exception) {
        ExceptionDescription exceptionDescription = new ExceptionDescription();
        exceptionDescription.setResponseCode(HttpStatus.NOT_FOUND.value());
        exceptionDescription.setErrorMessage(exception.getMessage());
        exceptionDescription.setResponseValue(HttpStatus.NOT_FOUND.getReasonPhrase());
        return new ResponseEntity<>(exceptionDescription, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ExceptionDescription> handleException(MethodArgumentNotValidException exception) {
        ExceptionDescription exceptionDescription = new ExceptionDescription();
        exceptionDescription.setResponseCode(HttpStatus.BAD_REQUEST.value());
        exceptionDescription.setErrorMessage(exception.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(", ")));
        exceptionDescription.setResponseValue(HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<>(exceptionDescription, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<ExceptionDescription> handleException(ConstraintViolationException exception) {
        ExceptionDescription exceptionDescription = new ExceptionDescription();
        exceptionDescription.setResponseCode(HttpStatus.BAD_REQUEST.value());
        exceptionDescription.setErrorMessage(exception.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining(", ")));
        exceptionDescription.setResponseValue(HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<>(exceptionDescription, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(JwtAuthenticationException.class)
    public ResponseEntity<ExceptionDescription> handleException(JwtAuthenticationException exception) {
        ExceptionDescription exceptionDescription = new ExceptionDescription();
        exceptionDescription.setResponseCode(HttpStatus.UNAUTHORIZED.value());
        exceptionDescription.setErrorMessage(exception.getMessage());
        exceptionDescription.setResponseValue(HttpStatus.UNAUTHORIZED.getReasonPhrase());
        return new ResponseEntity<>(exceptionDescription, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(IllegalActionException.class)
    public ResponseEntity<ExceptionDescription> handleException(IllegalActionException exception) {
        ExceptionDescription exceptionDescription = new ExceptionDescription();
        exceptionDescription.setResponseCode(HttpStatus.FORBIDDEN.value());
        exceptionDescription.setErrorMessage(exception.getMessage());
        exceptionDescription.setResponseValue(HttpStatus.FORBIDDEN.getReasonPhrase());
        return new ResponseEntity<>(exceptionDescription, HttpStatus.FORBIDDEN);
    }
}
