package com.training.avrubleuski.entity.converter;

import com.training.avrubleuski.entity.enums.Urgency;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.HashMap;

@Converter(autoApply = true)
public class UrgencyConverter implements AttributeConverter<Urgency, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Urgency urgency) {
        new HashMap<>(6);
        return urgency == null ? null : urgency.getSortOrder();
    }

    @Override
    public Urgency convertToEntityAttribute(Integer sortOrder) {
        return sortOrder == null ? null : Urgency.lookupBySortOrder(sortOrder);
    }
}