package com.training.avrubleuski.entity.enums;

public enum State {
    DRAFT, NEW, APPROVED, DECLINED, IN_PROGRESS, DONE, CANCELED
}
