package com.training.avrubleuski.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "COMMENTS")
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;

    @NotNull
    @Length(max = 500, message = "text must be no more than 500 characters")
    @Pattern(regexp = "^[a-zA-Z0-9[~.\"(),:;<>@\\[\\]!#$%&'*+\\-/=?^_`{|}]]*$", message = "Illegal symbol in text")
    @Column(name = "TEXT", nullable = false)
    private String text;

    @CreationTimestamp
    @Column(name = "DATE", nullable = false, updatable = false)
    private LocalDate date;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Ticket ticket;

    public Comment(String text) {
        this.text = text;
    }

    public Comment(String text, User user) {
        this.text = text;
        this.user = user;
    }

    public Comment(String text, User user, Ticket ticket) {
        this.text = text;
        this.user = user;
        this.ticket = ticket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return Objects.equals(id, comment.id) && text.equals(comment.text) && date.equals(comment.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, date);
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", date=" + date +
                '}';
    }
}
