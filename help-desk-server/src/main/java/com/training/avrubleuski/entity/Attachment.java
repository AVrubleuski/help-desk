package com.training.avrubleuski.entity;

import com.training.avrubleuski.listener.AttachmentListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "ATTACHMENTS")
@EntityListeners(AttachmentListener.class)
public class Attachment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false, updatable = false)
    private Long id;

    @Pattern(regexp = "^.+\\.(pdf|doc|docx|png|jpeg|jpg)$",
            message = "The selected file type is not allowed." +
                    " Please select a file of one of the following types: pdf, png, doc, docx, jpg, jpeg.")
    @Column(name = "NAME", nullable = false)
    private String name;

    @Lob
    @Column(name = "BLOB", columnDefinition = "BLOB", nullable = false)
    private byte[] blob;

    @ManyToOne(fetch = FetchType.LAZY)
    private Ticket ticket;

    public void addHistory(History history) {
        ticket.addHistory(history);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attachment that = (Attachment) o;
        return Objects.equals(id, that.id) && name.equals(that.name) && Arrays.equals(blob, that.blob);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, name);
        result = 31 * result + Arrays.hashCode(blob);
        return result;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
