package com.training.avrubleuski.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class TicketsListWrapper {

    private final List<Ticket> tickets;

    private final Long ticketsQuantity;

}
