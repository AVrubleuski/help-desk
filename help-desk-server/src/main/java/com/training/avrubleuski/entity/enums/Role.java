package com.training.avrubleuski.entity.enums;

public enum Role {
    EMPLOYEE, MANAGER, ENGINEER
}
