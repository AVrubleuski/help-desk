package com.training.avrubleuski.entity;

import com.training.avrubleuski.entity.enums.State;
import com.training.avrubleuski.entity.enums.Urgency;
import com.training.avrubleuski.listener.TicketListener;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "TICKETS")
@EntityListeners(TicketListener.class)
public class Ticket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @CreationTimestamp
    @Column(name = "CREATED_ON", nullable = false, updatable = false)
    private LocalDate createdOn;

    @Column(name = "DESIRED_RESOLUTION_DATE", nullable = false)
    private LocalDate desiredResolutionDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATE", nullable = false)
    private State state;

    @Column(name = "URGENCY", nullable = false)
    private Urgency urgency;

    @Column(name = "URGENCY_NAME", nullable = false)
    private String urgencyName;

    @ManyToOne(fetch = FetchType.LAZY)
    private User assignee;

    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY)
    private User approver;

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @OneToMany(
            mappedBy = "ticket",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Attachment> attachments = new HashSet<>();

    @OneToMany(
            mappedBy = "ticket",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<History> histories = new HashSet<>();

    @OneToMany(
            mappedBy = "ticket",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Comment> comments = new HashSet<>();

    public String getOwnerEmail() {
        return owner.getEmail();
    }

    public String getApproverEmail() {
        return approver.getEmail();
    }

    public String getAssigneeEmail() {
        return assignee.getEmail();
    }

    public boolean ownerIdEquals(Long userId) {
        return owner.getId().equals(userId);
    }

    public boolean approverIdEquals(Long userId) {
        return approver != null && approver.getId().equals(userId);
    }

    public boolean assigneeIdEquals(Long userId) {
        return assignee != null && assignee.getId().equals(userId);
    }

    public boolean stateEquals(State state) {
        return this.state.equals(state);
    }

    public void addAttachment(Attachment attachment) {
        this.attachments.add(attachment);
        attachment.setTicket(this);
    }

    public void removeAttachment(Attachment attachment) {
        this.attachments.remove(attachment);
        attachment.setTicket(null);
    }

    public void addHistory(History history) {
        this.histories.add(history);
        history.setTicket(this);
    }

    public void removeHistory(History history) {
        this.histories.remove(history);
        history.setTicket(null);
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
        comment.setTicket(this);
    }

    public void removeComment(Comment comment) {
        this.comments.remove(comment);
        comment.setTicket(null);
    }

    public void setUrgency(final Urgency urgency) {
        this.urgency = urgency;
        this.urgencyName = urgency.name();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(id, ticket.id) && description.equals(ticket.description) && createdOn.equals(ticket.createdOn) && desiredResolutionDate.equals(ticket.desiredResolutionDate) && state == ticket.state && urgency == ticket.urgency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, createdOn, desiredResolutionDate, state, urgency);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", createdOn=" + createdOn +
                ", desiredResolutionDate=" + desiredResolutionDate +
                ", state=" + state +
                ", urgency=" + urgency +
                '}';
    }
}
