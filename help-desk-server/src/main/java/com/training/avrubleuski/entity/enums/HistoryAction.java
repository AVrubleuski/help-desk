package com.training.avrubleuski.entity.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum HistoryAction {

    CREATED("Ticket is created"),
    EDITED("Ticket is edited"),
    CHANGED("Ticket status is changed"),
    ATTACHED("File is attached: "),
    REMOVED("File is removed: ");

    private final String actionDescription;
}
