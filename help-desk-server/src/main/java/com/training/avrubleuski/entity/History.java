package com.training.avrubleuski.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "HISTORIES")
public class History implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;

    @CreationTimestamp
    @Column(name = "DATE", nullable = false, updatable = false)
    private LocalDate date;

    @Column(name = "ACTION", nullable = false)
    private String action;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Ticket ticket;

    public History(String action, String description) {
        this.action = action;
        this.description = description;
    }

    public History(String action, String description, User user) {
        this.action = action;
        this.description = description;
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        History history = (History) o;
        return Objects.equals(id, history.id) && date.equals(history.date) && action.equals(history.action) && description.equals(history.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, action, description);
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", date=" + date +
                ", action='" + action + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
