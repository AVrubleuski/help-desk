package com.training.avrubleuski.entity.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public enum Urgency {
    AVERAGE(300), HIGH(200), CRITICAL(100), LOW(400);

    private final Integer sortOrder;

    private static final Map<Integer, Urgency> SortOrderIndex =
            new HashMap<>(Urgency.values().length);

    static {
        Arrays.stream(Urgency.values())
                .forEach(urgency -> SortOrderIndex.put(urgency.getSortOrder(), urgency));
    }

    public static Urgency lookupBySortOrder(Integer sortOrder) {
        return SortOrderIndex.getOrDefault(sortOrder, CRITICAL);
    }
}
