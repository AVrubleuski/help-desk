package com.training.avrubleuski.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.avrubleuski.dto.UserUserCredentialsDto;
import com.training.avrubleuski.exception.JwtAuthenticationException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final JWTConfig jwtConfig;

    public AuthenticationFilter(AuthenticationManager authenticationManager, JWTConfig jwtConfig) {
        this.authenticationManager = authenticationManager;
        this.jwtConfig = jwtConfig;
        setFilterProcessesUrl("/login");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try (ServletInputStream inputStream = request.getInputStream()) {
            UserUserCredentialsDto logUser = new ObjectMapper().readValue(inputStream, UserUserCredentialsDto.class);
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(logUser.getEmail(), logUser.getPassword(), new ArrayList<>()));
        } catch (IOException exception) {
            throw new JwtAuthenticationException("Fail read value from request");
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain, Authentication authentication) {
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        long validityTime = System.currentTimeMillis() + jwtConfig.getMilliseconds();
        String token = Jwts.builder()
                .setSubject(customUserDetails.getUsername())
                .setExpiration(new Date(validityTime))
                .signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret().getBytes())
                .compact();
        response.addHeader(jwtConfig.getAuthorizationHeader(), jwtConfig.getTokenPrefix() + " " + token);
    }
}
