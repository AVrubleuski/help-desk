package com.training.avrubleuski.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:jwt.properties")
public class JWTConfig {

    @Value("${jwt.expired.milliseconds}")
    private long milliseconds;
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.prefix}")
    private String tokenPrefix;
    @Value("${jwt.header}")
    private String authorizationHeader;

    public long getMilliseconds() {
        return milliseconds;
    }

    public String getSecret() {
        return secret;
    }

    public String getTokenPrefix() {
        return tokenPrefix;
    }

    public String getAuthorizationHeader() {
        return authorizationHeader;
    }
}
