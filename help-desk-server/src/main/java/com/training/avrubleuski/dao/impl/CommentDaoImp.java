package com.training.avrubleuski.dao.impl;

import com.training.avrubleuski.dao.CommentDao;
import com.training.avrubleuski.entity.Comment;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDaoImp extends AbstractCrudDao<Comment> implements CommentDao {
}
