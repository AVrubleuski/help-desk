package com.training.avrubleuski.dao;

import com.training.avrubleuski.entity.Comment;

public interface CommentDao extends CrudDao<Comment> {
}
