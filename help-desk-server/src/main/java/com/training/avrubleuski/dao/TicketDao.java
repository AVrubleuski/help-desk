package com.training.avrubleuski.dao;

import com.training.avrubleuski.dto.SearchParamDTO;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.TicketsListWrapper;


public interface TicketDao extends CrudDao<Ticket> {

    TicketsListWrapper getEmployeeTickets(Long employeeId, SearchParamDTO parameters);

    TicketsListWrapper getManagerMyTickets(Long managerId, SearchParamDTO parameters);

    TicketsListWrapper getManagerTickets(Long managerId, SearchParamDTO parameters);

    TicketsListWrapper getEngineerMyTickets(Long engineerId, SearchParamDTO parameters);

    TicketsListWrapper getEngineerTickets(Long engineerId, SearchParamDTO parameters);
}
