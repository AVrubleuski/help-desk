package com.training.avrubleuski.dao;

import com.training.avrubleuski.entity.History;

public interface HistoryDao extends CrudDao<History> {

}
