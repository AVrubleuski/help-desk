package com.training.avrubleuski.dao;

import com.training.avrubleuski.entity.Category;

public interface CategoryDao extends CrudDao<Category> {
}
