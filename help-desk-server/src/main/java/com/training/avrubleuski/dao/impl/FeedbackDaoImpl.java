package com.training.avrubleuski.dao.impl;

import com.training.avrubleuski.dao.FeedbackDao;
import com.training.avrubleuski.entity.Feedback;
import org.springframework.stereotype.Repository;

@Repository
public class FeedbackDaoImpl extends AbstractCrudDao<Feedback> implements FeedbackDao {
}
