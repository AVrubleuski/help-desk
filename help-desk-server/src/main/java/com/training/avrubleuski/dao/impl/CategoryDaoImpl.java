package com.training.avrubleuski.dao.impl;

import com.training.avrubleuski.dao.CategoryDao;
import com.training.avrubleuski.entity.Category;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDaoImpl extends AbstractCrudDao<Category> implements CategoryDao {
}
