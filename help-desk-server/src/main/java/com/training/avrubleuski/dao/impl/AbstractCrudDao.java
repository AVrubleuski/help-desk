package com.training.avrubleuski.dao.impl;

import com.training.avrubleuski.dao.CrudDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

abstract class AbstractCrudDao<T extends Serializable> implements CrudDao<T> {

    private final Class<T> clazz;

    @PersistenceContext
    protected EntityManager entityManager;

    protected AbstractCrudDao() {
        clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected AbstractCrudDao(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T saveEntity(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public T updateEntity(T entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void deleteEntity(T entity) {
        entityManager.remove(entity);
    }

    @Override
    public Optional<T> getEntityById(Long entityId) {
        return Optional.ofNullable(entityManager.find(clazz, entityId));
    }

    public List<T> getEntities(Long id, String query, String fieldName) {
        return entityManager.createQuery(query, clazz)
                .setParameter(fieldName, id)
                .getResultList();
    }

    @Override
    public List<T> getTicketEntities(Long ticketId) {
        return entityManager.createQuery(
                String.format("from %s e where e.ticket.id = :ticketId order by e.date ", clazz.getSimpleName()), clazz)
                .setParameter("ticketId", ticketId)
                .getResultList();
    }
}
