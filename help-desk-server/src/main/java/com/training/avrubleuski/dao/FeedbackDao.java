package com.training.avrubleuski.dao;

import com.training.avrubleuski.entity.Feedback;

public interface FeedbackDao extends CrudDao<Feedback> {
}
