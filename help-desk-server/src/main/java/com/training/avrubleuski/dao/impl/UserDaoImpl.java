package com.training.avrubleuski.dao.impl;

import com.training.avrubleuski.dao.UserDao;
import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDaoImpl extends AbstractCrudDao<User> implements UserDao {

    private static final String SELECT_USERS_BY_ROLE_STATEMENT =
            "from User u where u.role = :role";

    private static final String SELECT_USER_BY_EMAIL_STATEMENT =
            "from User u where u.email = :email";

    @Override
    public List<User> getUsersByRole(Role role) {
        return entityManager.createQuery(SELECT_USERS_BY_ROLE_STATEMENT, User.class)
                .setParameter("role", role)
                .getResultList();
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return Optional.ofNullable(entityManager.createQuery(SELECT_USER_BY_EMAIL_STATEMENT, User.class)
                .setParameter("email", email)
                .getSingleResult());
    }


}
