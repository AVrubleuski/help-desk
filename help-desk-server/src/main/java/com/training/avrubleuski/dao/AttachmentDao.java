package com.training.avrubleuski.dao;

import com.training.avrubleuski.entity.Attachment;

public interface AttachmentDao extends CrudDao<Attachment> {
}
