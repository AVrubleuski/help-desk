package com.training.avrubleuski.dao.impl;

import com.training.avrubleuski.dao.HistoryDao;
import com.training.avrubleuski.entity.History;
import org.springframework.stereotype.Repository;

@Repository
public class HistoryDaoImpl extends AbstractCrudDao<History> implements HistoryDao {
}
