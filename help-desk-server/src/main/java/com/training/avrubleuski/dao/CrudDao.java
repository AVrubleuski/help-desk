package com.training.avrubleuski.dao;

import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {

    T saveEntity(T entity);

    T updateEntity(T entity);

    void deleteEntity(T entity);

    Optional<T> getEntityById(Long entityId);

    List<T> getEntities(Long id, String query, String fieldName);

    List<T> getTicketEntities(Long ticketId);
}
