package com.training.avrubleuski.dao.impl;

import com.training.avrubleuski.dao.AttachmentDao;
import com.training.avrubleuski.entity.Attachment;
import org.springframework.stereotype.Repository;

@Repository
public class AttachmentDaoImpl extends AbstractCrudDao<Attachment> implements AttachmentDao {

}
