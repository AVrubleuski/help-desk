package com.training.avrubleuski.dao;

import com.training.avrubleuski.entity.User;
import com.training.avrubleuski.entity.enums.Role;

import java.util.List;
import java.util.Optional;

public interface UserDao extends CrudDao<User> {

    List<User> getUsersByRole(Role role);

    Optional<User> getUserByEmail(String email);
}
