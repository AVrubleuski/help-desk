package com.training.avrubleuski.dao.impl;

import com.training.avrubleuski.dao.TicketDao;
import com.training.avrubleuski.dto.SearchParamDTO;
import com.training.avrubleuski.entity.Ticket;
import com.training.avrubleuski.entity.TicketsListWrapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TicketDaoImpl extends AbstractCrudDao<Ticket> implements TicketDao {

    private static final String SELECT_USER_MY_TICKETS_HQL_STATEMENT =
            "from Ticket t where t.owner.id = :userId";

    private static final String SELECT_MANAGER_MY_TICKETS_HQL_STATEMENT =
            "from Ticket t where (t.owner.id = :userId" +
                    " or (t.approver.id= :userId and t.state= 'APPROVED'))";

    private static final String SELECT_MANAGER_TICKETS_HQL_STATEMENT =
            "from Ticket t where (t.owner.id = :userId" +
                    " or (t.state='NEW' and t.owner.role='EMPLOYEE')" +
                    " or (t.approver.id= :userId and t.state in ('APPROVED','DECLINED','CANCELLED','IN_PROGRESS','DONE')))";

    private static final String SELECT_ENGINEER_MY_TICKETS_HQL_STATEMENT =
            "from Ticket t where t.assignee.id= :userId and t.state in ('IN_PROGRESS','DONE')";

    private static final String SELECT_ENGINEER_TICKETS_HQL_STATEMENT =
            "from Ticket t where (t.state='APPROVED'" +
                    " or (t.assignee.id= :userId and t.state in ('IN_PROGRESS','DONE')))";

    private static final String FILTER_TICKETS_HQL_STATEMENT =
            "and ( cast(t.id as string) like :filterValue" +
                    " or t.name like :filterValue" +
                    " or t.desiredResolutionDate like :filterValue" +
                    " or t.urgencyName like :filterValue" +
                    " or t.state like :filterValue )";

    @Override
    public TicketsListWrapper getEmployeeTickets(Long employeeId, SearchParamDTO parameters) {
        return getTicketsListWrapper(SELECT_USER_MY_TICKETS_HQL_STATEMENT, employeeId, parameters);
    }

    @Override
    public TicketsListWrapper getManagerMyTickets(Long managerId, SearchParamDTO parameters) {
        return getTicketsListWrapper(SELECT_MANAGER_MY_TICKETS_HQL_STATEMENT, managerId, parameters);
    }

    @Override
    public TicketsListWrapper getManagerTickets(Long managerId, SearchParamDTO parameters) {
        return getTicketsListWrapper(SELECT_MANAGER_TICKETS_HQL_STATEMENT, managerId, parameters);
    }

    @Override
    public TicketsListWrapper getEngineerMyTickets(Long engineerId, SearchParamDTO parameters) {
        return getTicketsListWrapper(SELECT_ENGINEER_MY_TICKETS_HQL_STATEMENT, engineerId, parameters);
    }

    @Override
    public TicketsListWrapper getEngineerTickets(Long engineerId, SearchParamDTO parameters) {
        return getTicketsListWrapper(SELECT_ENGINEER_TICKETS_HQL_STATEMENT, engineerId, parameters);
    }

    private TicketsListWrapper getTicketsListWrapper(String query, Long userId, SearchParamDTO parameters) {
        return new TicketsListWrapper(getTickets(query, userId, parameters),
                getTicketsQuantity(query, userId, parameters));
    }

    private List<Ticket> getTickets(String query, Long userId, SearchParamDTO parameters) {
        return entityManager.createQuery(String.format("%s %s order by t.%s %s",
                query,
                FILTER_TICKETS_HQL_STATEMENT,
                parameters.getSortField().getFieldName(),
                parameters.getSortDirection()), Ticket.class)
                .setParameter("userId", userId)
                .setParameter("filterValue", "%" + parameters.getFilterValue() + "%")
                .setFirstResult(parameters.getFirstResult())
                .setMaxResults(parameters.getPageSize())
                .getResultList();
    }

    private Long getTicketsQuantity(String query, Long userId, SearchParamDTO parameters) {
        return (Long) entityManager.createQuery(String.format("select count(*) %s %s",
                query,
                FILTER_TICKETS_HQL_STATEMENT))
                .setParameter("userId", userId)
                .setParameter("filterValue", "%" + parameters.getFilterValue() + "%")
                .getSingleResult();
    }
}
